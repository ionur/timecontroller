﻿Windows servisin ozellikleri

1) 10 dk'da bir calisir.
2) Hesap kitap islerini yaparak EnployeeDailyActions tablosunu doldurur.
3) C:\temp altında TCCalculatorService.txt adli bir dosyaya calisma zamani hakkinda log atar. 
4) Job kendi loglarini JobLogs tablosunda tutar. Eger bir hata varsa description alaninda yazar.


Yuklemek Icin
1) command prompt'u Administrator yetkileri ile calistir
2) cmd'de TimeControllerService.exe dosyasinin bulundugu dizine gir.
3) cmd'de ilgili dizinde bulunan install.bat dosyasini calistir. OR : C:\Temp>install.bat
4) cmd ekraninda herhangi bir hata var mi kontrol et.
5) Sonra bilgisayarima sag tikla ve yonet de.
6) Service'leri listele. "Time Controller Labour Calculator Service" isimli servisin oldugundan emin ol
7) Eger servis Automatic ve Start olmussa install basarilidir.

Kaldirmak Icin
1) command prompt'u Administrator yetkileri ile calistir
2) cmd'de TimeControllerService.exe dosyasinin bulundugu dizine gir.
3) cmd'de ilgili dizinde bulunan install.bat dosyasini calistir. OR : C:\Temp>uninstall.bat