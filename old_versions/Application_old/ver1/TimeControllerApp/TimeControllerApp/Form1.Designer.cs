﻿namespace TimeControllerApp
{
    partial class MaxForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing); 
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.barcodebox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonApprove = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOutSpecial = new System.Windows.Forms.Button();
            this.buttonOutLunch = new System.Windows.Forms.Button();
            this.buttonInSpecial = new System.Windows.Forms.Button();
            this.buttonInMorning = new System.Windows.Forms.Button();
            this.buttonOut = new System.Windows.Forms.Button();
            this.buttonIn = new System.Windows.Forms.Button();
            this.labelShowTime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.barcodebox);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.buttonApprove);
            this.groupBox1.Controls.Add(this.buttonCancel);
            this.groupBox1.Controls.Add(this.buttonOutSpecial);
            this.groupBox1.Controls.Add(this.buttonOutLunch);
            this.groupBox1.Controls.Add(this.buttonInSpecial);
            this.groupBox1.Controls.Add(this.buttonInMorning);
            this.groupBox1.Controls.Add(this.buttonOut);
            this.groupBox1.Controls.Add(this.buttonIn);
            this.groupBox1.Controls.Add(this.labelShowTime);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1264, 762);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(569, 613);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 46);
            this.label1.TabIndex = 16;
            this.label1.Text = "label1";
            // 
            // barcodebox
            // 
            this.barcodebox.Location = new System.Drawing.Point(533, 538);
            this.barcodebox.Name = "barcodebox";
            this.barcodebox.Size = new System.Drawing.Size(198, 20);
            this.barcodebox.TabIndex = 15;
            this.barcodebox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaxForm_KeyPress);
            this.barcodebox.Leave += new System.EventHandler(this.barcodebox_Leave);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(24, 683);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "Full Screen";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(24, 712);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Normal";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonApprove
            // 
            this.buttonApprove.Location = new System.Drawing.Point(533, 411);
            this.buttonApprove.Name = "buttonApprove";
            this.buttonApprove.Size = new System.Drawing.Size(198, 46);
            this.buttonApprove.TabIndex = 12;
            this.buttonApprove.Text = "Approve";
            this.buttonApprove.UseVisualStyleBackColor = true;
            this.buttonApprove.Click += new System.EventHandler(this.buttonApprove_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(533, 463);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(198, 46);
            this.buttonCancel.TabIndex = 11;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOutSpecial
            // 
            this.buttonOutSpecial.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonOutSpecial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOutSpecial.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonOutSpecial.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonOutSpecial.Location = new System.Drawing.Point(737, 280);
            this.buttonOutSpecial.Name = "buttonOutSpecial";
            this.buttonOutSpecial.Size = new System.Drawing.Size(163, 103);
            this.buttonOutSpecial.TabIndex = 6;
            this.buttonOutSpecial.Text = "Special";
            this.buttonOutSpecial.UseVisualStyleBackColor = false;
            this.buttonOutSpecial.Click += new System.EventHandler(this.buttonOutSpecial_Click);
            // 
            // buttonOutLunch
            // 
            this.buttonOutLunch.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonOutLunch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOutLunch.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonOutLunch.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonOutLunch.Location = new System.Drawing.Point(737, 177);
            this.buttonOutLunch.Name = "buttonOutLunch";
            this.buttonOutLunch.Size = new System.Drawing.Size(163, 97);
            this.buttonOutLunch.TabIndex = 5;
            this.buttonOutLunch.Text = "Lunch";
            this.buttonOutLunch.UseVisualStyleBackColor = false;
            this.buttonOutLunch.Click += new System.EventHandler(this.buttonOutLunch_Click);
            // 
            // buttonInSpecial
            // 
            this.buttonInSpecial.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonInSpecial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInSpecial.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonInSpecial.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonInSpecial.Location = new System.Drawing.Point(364, 280);
            this.buttonInSpecial.Name = "buttonInSpecial";
            this.buttonInSpecial.Size = new System.Drawing.Size(163, 103);
            this.buttonInSpecial.TabIndex = 4;
            this.buttonInSpecial.Text = "Special";
            this.buttonInSpecial.UseVisualStyleBackColor = false;
            this.buttonInSpecial.Click += new System.EventHandler(this.buttonInSpecial_Click);
            // 
            // buttonInMorning
            // 
            this.buttonInMorning.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonInMorning.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInMorning.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonInMorning.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonInMorning.Location = new System.Drawing.Point(364, 177);
            this.buttonInMorning.Name = "buttonInMorning";
            this.buttonInMorning.Size = new System.Drawing.Size(163, 97);
            this.buttonInMorning.TabIndex = 3;
            this.buttonInMorning.Text = "Morning";
            this.buttonInMorning.UseVisualStyleBackColor = false;
            this.buttonInMorning.Click += new System.EventHandler(this.buttonInMorning_Click);
            // 
            // buttonOut
            // 
            this.buttonOut.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonOut.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonOut.Location = new System.Drawing.Point(906, 177);
            this.buttonOut.Name = "buttonOut";
            this.buttonOut.Size = new System.Drawing.Size(141, 206);
            this.buttonOut.TabIndex = 2;
            this.buttonOut.Text = "OUT";
            this.buttonOut.UseVisualStyleBackColor = false;
            this.buttonOut.Click += new System.EventHandler(this.buttonOut_Click);
            // 
            // buttonIn
            // 
            this.buttonIn.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonIn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonIn.Location = new System.Drawing.Point(217, 177);
            this.buttonIn.Name = "buttonIn";
            this.buttonIn.Size = new System.Drawing.Size(141, 206);
            this.buttonIn.TabIndex = 1;
            this.buttonIn.Text = "IN";
            this.buttonIn.UseVisualStyleBackColor = false;
            this.buttonIn.Click += new System.EventHandler(this.buttonIn_Click);
            // 
            // labelShowTime
            // 
            this.labelShowTime.AutoSize = true;
            this.labelShowTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelShowTime.Location = new System.Drawing.Point(550, 46);
            this.labelShowTime.Name = "labelShowTime";
            this.labelShowTime.Size = new System.Drawing.Size(165, 31);
            this.labelShowTime.TabIndex = 0;
            this.labelShowTime.Text = "CurrentTime";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MaxForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1264, 762);
            this.Controls.Add(this.groupBox1);
            this.Name = "MaxForm";
            this.Text = "Time Controller";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.MaxForm_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelShowTime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button buttonOut;
        private System.Windows.Forms.Button buttonIn;
        private System.Windows.Forms.Button buttonInSpecial;
        private System.Windows.Forms.Button buttonInMorning;
        private System.Windows.Forms.Button buttonOutSpecial;
        private System.Windows.Forms.Button buttonOutLunch;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox barcodebox;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Button buttonApprove;
    }
}

