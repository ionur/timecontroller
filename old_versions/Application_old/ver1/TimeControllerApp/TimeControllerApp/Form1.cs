﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Org.Vesic.WinForms;

namespace TimeControllerApp
{



    public partial class MaxForm : Form
    {
        timeMode[] outmodes = new timeMode[5];
        timeMode[] inmodes = new timeMode[5];
        ActionButton[] actionbuttons;
        timeMode nonspecial;
        FormState formState = new FormState();

        public void showspecialbuttons(bool show, int exceptid = -1)
        {
            if (actionbuttons != null)
            {
                foreach (ActionButton a in actionbuttons)
                {
                    if (show == true) a.Visible = show;
                    else if (a.actionid != exceptid)
                    {
                        this.Controls.Remove(a);
                        a.Dispose();
                    }
                }
            }

        }

        public int actionid;
        public int statusid;
        public int userid;

        int userActionID = 0;
        int actionID = 0;
        int specialLogin = 0;
        int specialLogout = 0;

        public MaxForm()
        {
            InitializeComponent();
            formState.Maximize(this);
            barcodebox.Focus();
            Console.Write("");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'timeControllerDBDataSet.Users' table. You can move, or remove it, as needed.
            labelShowTime.Text = "";
            timer1.Start();

            ResetButtons();
        }

        private void ResetButtons()
        {
            label1.Text = "";
            label1.Hide();
            buttonIn.Visible = true;
            buttonOut.Visible = true;
            buttonInMorning.Visible = false;
            buttonInSpecial.Visible = false;
            buttonOutLunch.Visible = false;
            buttonOutSpecial.Visible = false;


            buttonApprove.Visible = false;

            buttonIn.Enabled = false;
            buttonOut.Enabled = false;
            buttonInMorning.Enabled = true;
            buttonInSpecial.Enabled = true;
            buttonOutLunch.Enabled = true;
            buttonOutSpecial.Enabled = true;

            showspecialbuttons(false);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //labelShowTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
            /*int hours = DateTime.Now.Hour;
            int minutes = DateTime.Now.Minute;
            int seconds = DateTime.Now.Second;
            int milSeconds = DateTime.Now.Millisecond;

            string timeString = hours + " : " + minutes + " : " + seconds + " : " + milSeconds;

            labelShowTime.Text = timeString;*/
            labelShowTime.Text = System.DateTime.Now.ToLongTimeString();
        }

        private void createbuttons(String mode)
        {
            int now = DateTime.Now.Hour * 60 + DateTime.Now.Minute;
            String query;

            if (mode == "in") query = "Select * From dbo.Actions Where StatusID = 1 And StartTime < " + now + " AND EndTime>" + now;
            else query = "Select * From dbo.Actions Where StatusID != 1 And StartTime < " + now + " AND EndTime>" + now;

            DataSet dsinmodes = DB.Sorgula(query, "");
            inmodes = new timeMode[dsinmodes.Tables[0].Rows.Count];
            int i = 0;
            foreach (DataRow row in dsinmodes.Tables[0].Rows)
            {

                inmodes[i] = new timeMode((int)row["ActionID"], (int)row["StatusID"], row["Title"].ToString(), (int)row["Special"]);
                i++;
            }

            int specialcount = 0;
            foreach (timeMode tm in inmodes)
            {
                if (tm.special == 0)
                {
                    buttonInMorning.Text = tm.title;
                    nonspecial = tm;
                }
                else
                {
                    specialcount++;
                }
            }

            actionbuttons = new ActionButton[specialcount];
            int k = 0;
            foreach (timeMode tm in inmodes)
            {
                if (tm.special == 1)
                {
                    actionbuttons[k] = new ActionButton(this, tm.id, tm.status, (176 + (208 / specialcount) * k), (208 / specialcount) - 1, tm.title);
                    k++;
                }
            }
        }

        private void buttonIn_Click(object sender, EventArgs e)
        {
            createbuttons("in");
            buttonIn.Visible = true;
            buttonOut.Visible = false;
            buttonInMorning.Visible = true;
            buttonInSpecial.Visible = true;
            buttonOutLunch.Visible = false;
            buttonOutSpecial.Visible = false;
            //showspecialbuttons(false);

            buttonIn.Enabled = false;
        }

        private void buttonInMorning_Click(object sender, EventArgs e)
        {
            actionid = nonspecial.id;
            statusid = nonspecial.status;

            buttonIn.Visible = true;
            buttonOut.Visible = false;
            buttonInMorning.Visible = true;
            buttonInSpecial.Visible = false;
            buttonOutLunch.Visible = false;
            buttonOutSpecial.Visible = false;


            buttonApprove.Visible = true;

            buttonIn.Enabled = false;
            showspecialbuttons(false);
            buttonInMorning.Enabled = false;
        }

        private void buttonInSpecial_Click(object sender, EventArgs e)
        {
            foreach (ActionButton a in actionbuttons)
            {
                a.Visible = true;
            }
            buttonIn.Visible = true;
            buttonOut.Visible = false;
            buttonInMorning.Visible = false;
            buttonInSpecial.Visible = true;
            buttonOutLunch.Visible = false;
            buttonOutSpecial.Visible = false;
            showspecialbuttons(true);

            buttonIn.Enabled = false;
            buttonInSpecial.Enabled = false;
        }

        private void buttonCustomerVendor_Click(object sender, EventArgs e)
        {

            buttonApprove.Visible = true;
        }

        private void buttonPersonal_Click(object sender, EventArgs e)
        {

            buttonApprove.Visible = true;
        }

        private void buttonSpecialDoctor_Click(object sender, EventArgs e)
        {


            buttonApprove.Visible = true;
        }

        private void buttonGeneralDoctor_Click(object sender, EventArgs e)
        {


            buttonApprove.Visible = true;
        }

        private void buttonOut_Click(object sender, EventArgs e)
        {
            createbuttons("out");
            buttonIn.Visible = false;
            buttonOut.Visible = true;
            buttonInMorning.Visible = false;
            buttonInSpecial.Visible = false;
            buttonOutLunch.Visible = true;
            buttonOutSpecial.Visible = true;

            buttonOut.Enabled = false;
        }

        private void buttonOutLunch_Click(object sender, EventArgs e)
        {
            actionid = nonspecial.id;
            statusid = nonspecial.status;
            buttonIn.Visible = false;
            buttonOut.Visible = true;
            buttonInMorning.Visible = false;
            buttonInSpecial.Visible = false;
            buttonOutLunch.Visible = true;
            buttonOutSpecial.Visible = false;

            buttonApprove.Visible = true;

            buttonOut.Enabled = false;
            buttonOutLunch.Enabled = false;
        }

        private void buttonOutSpecial_Click(object sender, EventArgs e)
        {
            buttonIn.Visible = false;
            buttonOut.Visible = true;
            buttonInMorning.Visible = false;
            buttonInSpecial.Visible = false;
            buttonOutLunch.Visible = false;
            buttonOutSpecial.Visible = true;

            showspecialbuttons(true);
            buttonOut.Enabled = false;
            buttonOutSpecial.Enabled = false;

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            ResetButtons();
        }

        private void buttonApprove_Click(object sender, EventArgs e)
        {
            bool useraction = DB.Uygula("INSERT INTO dbo.EmployeeActions (UserID,ActionID,ActionTime) VALUES (" + userid + "," + actionid + ",GETDATE())", "");
            if (useraction == true)
            {
                bool userinfo = DB.Uygula("UPDATE dbo.Employees SET StatusID =" + statusid + " WHERE EmployeeID = " + userid, "");
            }
            ResetButtons();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            formState.Restore(this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            formState.Maximize(this);
        }

        private void barcodebox_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void MaxForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                String code = barcodebox.Text.Trim();
                DataSet dsuser = DB.Sorgula("SELECT * FROM dbo.Employees WHERE Code = '" + code + "';", "");
                barcodebox.Text = "";
                if (dsuser != null || dsuser.Tables.Count > 0)
                {
                    userid = (int)dsuser.Tables[0].Rows[0]["EmployeeID"];
                    label1.Text = dsuser.Tables[0].Rows[0]["NameSurname"].ToString();
                    label1.Show();
                    buttonIn.Enabled = true;
                    buttonOut.Enabled = true;
                }
            }
        }

        private void buttonApprove_Click_1(object sender, EventArgs e)
        {
            barcodebox.Focus();

        }

        private void MaxForm_Shown(object sender, EventArgs e)
        {
            barcodebox.Focus();
        }

        private void barcodebox_Leave(object sender, EventArgs e)
        {
            barcodebox.Focus();
        }



    }

    class timeMode
    {
        public int id;
        public int status;
        public String title;
        public int special;

        public timeMode(int id, int status, String name, int special)
        {
            this.id = id;
            this.status = status;
            this.title = name;
            this.special = special;
        }
    }

    class ActionButton : Button
    {
        public int actionid;
        public int statusid;
        MaxForm form;
        public ActionButton(MaxForm frm, int actionid, int statusid, int y, int height, String title)
            : base()
        {
            this.form = frm;
            this.actionid = actionid;
            this.statusid = statusid;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Location = new System.Drawing.Point(533, y);
            this.Size = new System.Drawing.Size(198, height);
            this.Text = title;
            this.UseVisualStyleBackColor = false;
            this.Click += new System.EventHandler(this.buttonClick);
            this.Visible = false;
            frm.groupBox1.Controls.Add(this);
        }

        private void buttonClick(object sender, EventArgs e)
        {
            form.actionid = this.actionid;
            form.statusid = this.statusid;
            form.buttonApprove.Visible = true;
            form.showspecialbuttons(false, this.actionid);
        }
    }

}
