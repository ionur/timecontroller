﻿namespace TimeControllerAdminApp
{
    partial class FormAdminMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdminMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.DataGridViewEmployee = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.GroupBoxEmployeeVacations = new System.Windows.Forms.GroupBox();
            this.LabelRemDayCount = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.LabelUsedDayCount = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.DateTimePickerVacationTo = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.DateTimePickerVacationFrom = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.ButtonNewVacation = new System.Windows.Forms.Button();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.ButtonDeleteVacation = new System.Windows.Forms.Button();
            this.ButtonSaveVacation = new System.Windows.Forms.Button();
            this.DataGridViewEmployeeVacations = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.NumericUpDownVacLimit = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.TextBoxEmployeeName = new System.Windows.Forms.TextBox();
            this.ComboBoxEmployeeDept = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.ComboBoxEmployeeManager = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TextBoxEmployeeCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ButtonNewEmployee = new System.Windows.Forms.Button();
            this.TextBoxEmployeePhone = new System.Windows.Forms.TextBox();
            this.ButtonDeleteEmployee = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ButtonSaveEmployee = new System.Windows.Forms.Button();
            this.TextBoxEmployeeEmail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.DataGridViewDateOfDays = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.DataGridViewEmployeeActions = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.DataGridViewTotalTime = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataGridViewDailyActions = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.CheckBoxPeriodFilter1 = new System.Windows.Forms.CheckBox();
            this.CheckBoxPeriodFilter2 = new System.Windows.Forms.CheckBox();
            this.GroupBoxDate = new System.Windows.Forms.GroupBox();
            this.DateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.DateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.ButtonSubmit = new System.Windows.Forms.Button();
            this.GroupBoxPeriod = new System.Windows.Forms.GroupBox();
            this.RadioButtonYear = new System.Windows.Forms.RadioButton();
            this.RadioButtonWeek = new System.Windows.Forms.RadioButton();
            this.RadioButtonMonth = new System.Windows.Forms.RadioButton();
            this.RadioButtonAll = new System.Windows.Forms.RadioButton();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.DataGridViewEmployeeList = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.DataGridViewActions = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.DataGridViewPossibleActions = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DayTypeContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.fullDayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.halfDayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.holidayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewEmployee)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.GroupBoxEmployeeVacations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewEmployeeVacations)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownVacLimit)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDateOfDays)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewEmployeeActions)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewTotalTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDailyActions)).BeginInit();
            this.panel4.SuspendLayout();
            this.GroupBoxDate.SuspendLayout();
            this.GroupBoxPeriod.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewEmployeeList)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewActions)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewPossibleActions)).BeginInit();
            this.DayTypeContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1008, 662);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1000, 636);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = " Employee Definition";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Khaki;
            this.groupBox5.Controls.Add(this.DataGridViewEmployee);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(994, 389);
            this.groupBox5.TabIndex = 113;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = " Employees  ";
            // 
            // DataGridViewEmployee
            // 
            this.DataGridViewEmployee.AllowUserToAddRows = false;
            this.DataGridViewEmployee.AllowUserToOrderColumns = true;
            this.DataGridViewEmployee.AllowUserToResizeRows = false;
            this.DataGridViewEmployee.BackgroundColor = System.Drawing.Color.White;
            this.DataGridViewEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewEmployee.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn8,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.DataGridViewEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewEmployee.EnableHeadersVisualStyles = false;
            this.DataGridViewEmployee.Location = new System.Drawing.Point(3, 17);
            this.DataGridViewEmployee.MultiSelect = false;
            this.DataGridViewEmployee.Name = "DataGridViewEmployee";
            this.DataGridViewEmployee.ReadOnly = true;
            this.DataGridViewEmployee.RowHeadersWidth = 20;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.DataGridViewEmployee.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridViewEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewEmployee.Size = new System.Drawing.Size(988, 369);
            this.DataGridViewEmployee.TabIndex = 104;
            this.DataGridViewEmployee.SelectionChanged += new System.EventHandler(this.DataGridViewEmployee_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "ID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.HeaderText = "Name && Surname";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn8.HeaderText = "Status";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 63;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Code";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "EMail";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column3.HeaderText = "Phone Number";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 94;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Manager";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Department";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox2.Controls.Add(this.GroupBoxEmployeeVacations);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(3, 392);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(994, 241);
            this.groupBox2.TabIndex = 122;
            this.groupBox2.TabStop = false;
            // 
            // GroupBoxEmployeeVacations
            // 
            this.GroupBoxEmployeeVacations.Controls.Add(this.LabelRemDayCount);
            this.GroupBoxEmployeeVacations.Controls.Add(this.label14);
            this.GroupBoxEmployeeVacations.Controls.Add(this.LabelUsedDayCount);
            this.GroupBoxEmployeeVacations.Controls.Add(this.label12);
            this.GroupBoxEmployeeVacations.Controls.Add(this.label17);
            this.GroupBoxEmployeeVacations.Controls.Add(this.label7);
            this.GroupBoxEmployeeVacations.Controls.Add(this.DateTimePickerVacationTo);
            this.GroupBoxEmployeeVacations.Controls.Add(this.label6);
            this.GroupBoxEmployeeVacations.Controls.Add(this.DateTimePickerVacationFrom);
            this.GroupBoxEmployeeVacations.Controls.Add(this.label5);
            this.GroupBoxEmployeeVacations.Controls.Add(this.ButtonNewVacation);
            this.GroupBoxEmployeeVacations.Controls.Add(this.ButtonDeleteVacation);
            this.GroupBoxEmployeeVacations.Controls.Add(this.ButtonSaveVacation);
            this.GroupBoxEmployeeVacations.Controls.Add(this.DataGridViewEmployeeVacations);
            this.GroupBoxEmployeeVacations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupBoxEmployeeVacations.Location = new System.Drawing.Point(528, 17);
            this.GroupBoxEmployeeVacations.Name = "GroupBoxEmployeeVacations";
            this.GroupBoxEmployeeVacations.Size = new System.Drawing.Size(463, 221);
            this.GroupBoxEmployeeVacations.TabIndex = 123;
            this.GroupBoxEmployeeVacations.TabStop = false;
            this.GroupBoxEmployeeVacations.Text = "Vacaciones Trabajador";
            // 
            // LabelRemDayCount
            // 
            this.LabelRemDayCount.AutoSize = true;
            this.LabelRemDayCount.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelRemDayCount.ForeColor = System.Drawing.Color.RoyalBlue;
            this.LabelRemDayCount.Location = new System.Drawing.Point(156, 202);
            this.LabelRemDayCount.Name = "LabelRemDayCount";
            this.LabelRemDayCount.Size = new System.Drawing.Size(21, 13);
            this.LabelRemDayCount.TabIndex = 135;
            this.LabelRemDayCount.Text = "12";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(4, 202);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(146, 13);
            this.label14.TabIndex = 134;
            this.label14.Text = "Contador Vac. Restantes";
            // 
            // LabelUsedDayCount
            // 
            this.LabelUsedDayCount.AutoSize = true;
            this.LabelUsedDayCount.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelUsedDayCount.ForeColor = System.Drawing.Color.Red;
            this.LabelUsedDayCount.Location = new System.Drawing.Point(156, 189);
            this.LabelUsedDayCount.Name = "LabelUsedDayCount";
            this.LabelUsedDayCount.Size = new System.Drawing.Size(21, 13);
            this.LabelUsedDayCount.TabIndex = 133;
            this.LabelUsedDayCount.Text = "12";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(11, 189);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(143, 13);
            this.label12.TabIndex = 132;
            this.label12.Text = "Contador Vac. Utilizadas";
            // 
            // label17
            // 
            this.label17.AutoEllipsis = true;
            this.label17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label17.Location = new System.Drawing.Point(167, 157);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(290, 26);
            this.label17.TabIndex = 131;
            this.label17.Text = "** The date which employee starts to work after vacation";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoEllipsis = true;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Location = new System.Drawing.Point(173, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(216, 23);
            this.label7.TabIndex = 130;
            this.label7.Text = "* The date which employee starts vacation";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DateTimePickerVacationTo
            // 
            this.DateTimePickerVacationTo.CustomFormat = "yyyy.MM.dd";
            this.DateTimePickerVacationTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePickerVacationTo.Location = new System.Drawing.Point(64, 160);
            this.DateTimePickerVacationTo.Name = "DateTimePickerVacationTo";
            this.DateTimePickerVacationTo.Size = new System.Drawing.Size(95, 21);
            this.DateTimePickerVacationTo.TabIndex = 129;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(11, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 128;
            this.label6.Text = "Hasta **";
            // 
            // DateTimePickerVacationFrom
            // 
            this.DateTimePickerVacationFrom.CustomFormat = "yyyy.MM.dd";
            this.DateTimePickerVacationFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePickerVacationFrom.Location = new System.Drawing.Point(64, 134);
            this.DateTimePickerVacationFrom.Name = "DateTimePickerVacationFrom";
            this.DateTimePickerVacationFrom.Size = new System.Drawing.Size(95, 21);
            this.DateTimePickerVacationFrom.TabIndex = 127;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(16, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 126;
            this.label5.Text = "Desde *";
            // 
            // ButtonNewVacation
            // 
            this.ButtonNewVacation.ImageKey = "spreadsheet.ico";
            this.ButtonNewVacation.ImageList = this.imageList2;
            this.ButtonNewVacation.Location = new System.Drawing.Point(230, 183);
            this.ButtonNewVacation.Name = "ButtonNewVacation";
            this.ButtonNewVacation.Size = new System.Drawing.Size(75, 32);
            this.ButtonNewVacation.TabIndex = 125;
            this.ButtonNewVacation.Text = "New";
            this.ButtonNewVacation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonNewVacation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonNewVacation.UseVisualStyleBackColor = true;
            this.ButtonNewVacation.Click += new System.EventHandler(this.ButtonNewVacation_Click);
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "Search.ico");
            this.imageList2.Images.SetKeyName(1, "printer1.ico");
            this.imageList2.Images.SetKeyName(2, "password.ico");
            this.imageList2.Images.SetKeyName(3, "Access43535.ico");
            this.imageList2.Images.SetKeyName(4, "button_ok.ico");
            this.imageList2.Images.SetKeyName(5, "button_cancel.ico");
            this.imageList2.Images.SetKeyName(6, "Corel Designer Icon.ico");
            this.imageList2.Images.SetKeyName(7, "52.ico");
            this.imageList2.Images.SetKeyName(8, "restart.ico");
            this.imageList2.Images.SetKeyName(9, "spreadsheet.ico");
            this.imageList2.Images.SetKeyName(10, "Help.ico");
            this.imageList2.Images.SetKeyName(11, "Settings.png");
            // 
            // ButtonDeleteVacation
            // 
            this.ButtonDeleteVacation.ImageKey = "button_cancel.ico";
            this.ButtonDeleteVacation.ImageList = this.imageList2;
            this.ButtonDeleteVacation.Location = new System.Drawing.Point(384, 183);
            this.ButtonDeleteVacation.Name = "ButtonDeleteVacation";
            this.ButtonDeleteVacation.Size = new System.Drawing.Size(75, 32);
            this.ButtonDeleteVacation.TabIndex = 124;
            this.ButtonDeleteVacation.Text = "Delete";
            this.ButtonDeleteVacation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonDeleteVacation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonDeleteVacation.UseVisualStyleBackColor = true;
            this.ButtonDeleteVacation.Click += new System.EventHandler(this.ButtonDeleteVacation_Click);
            // 
            // ButtonSaveVacation
            // 
            this.ButtonSaveVacation.ImageKey = "button_ok.ico";
            this.ButtonSaveVacation.ImageList = this.imageList2;
            this.ButtonSaveVacation.Location = new System.Drawing.Point(307, 183);
            this.ButtonSaveVacation.Name = "ButtonSaveVacation";
            this.ButtonSaveVacation.Size = new System.Drawing.Size(75, 32);
            this.ButtonSaveVacation.TabIndex = 123;
            this.ButtonSaveVacation.Text = "Save";
            this.ButtonSaveVacation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonSaveVacation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonSaveVacation.UseVisualStyleBackColor = true;
            this.ButtonSaveVacation.Click += new System.EventHandler(this.ButtonSaveVacation_Click);
            // 
            // DataGridViewEmployeeVacations
            // 
            this.DataGridViewEmployeeVacations.AllowUserToAddRows = false;
            this.DataGridViewEmployeeVacations.AllowUserToOrderColumns = true;
            this.DataGridViewEmployeeVacations.AllowUserToResizeRows = false;
            this.DataGridViewEmployeeVacations.BackgroundColor = System.Drawing.Color.White;
            this.DataGridViewEmployeeVacations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewEmployeeVacations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.Column8});
            this.DataGridViewEmployeeVacations.Dock = System.Windows.Forms.DockStyle.Top;
            this.DataGridViewEmployeeVacations.EnableHeadersVisualStyles = false;
            this.DataGridViewEmployeeVacations.Location = new System.Drawing.Point(3, 17);
            this.DataGridViewEmployeeVacations.MultiSelect = false;
            this.DataGridViewEmployeeVacations.Name = "DataGridViewEmployeeVacations";
            this.DataGridViewEmployeeVacations.ReadOnly = true;
            this.DataGridViewEmployeeVacations.RowHeadersWidth = 20;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.DataGridViewEmployeeVacations.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridViewEmployeeVacations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewEmployeeVacations.Size = new System.Drawing.Size(457, 107);
            this.DataGridViewEmployeeVacations.TabIndex = 122;
            this.DataGridViewEmployeeVacations.SelectionChanged += new System.EventHandler(this.DataGridViewEmployeeVacations_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "From";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "To";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Day Count";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.NumericUpDownVacLimit);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.TextBoxEmployeeName);
            this.groupBox1.Controls.Add(this.ComboBoxEmployeeDept);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.ComboBoxEmployeeManager);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.TextBoxEmployeeCode);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ButtonNewEmployee);
            this.groupBox1.Controls.Add(this.TextBoxEmployeePhone);
            this.groupBox1.Controls.Add(this.ButtonDeleteEmployee);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.ButtonSaveEmployee);
            this.groupBox1.Controls.Add(this.TextBoxEmployeeEmail);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(3, 17);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(525, 221);
            this.groupBox1.TabIndex = 121;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informacion Trabajador";
            // 
            // NumericUpDownVacLimit
            // 
            this.NumericUpDownVacLimit.Location = new System.Drawing.Point(109, 124);
            this.NumericUpDownVacLimit.Name = "NumericUpDownVacLimit";
            this.NumericUpDownVacLimit.Size = new System.Drawing.Size(41, 21);
            this.NumericUpDownVacLimit.TabIndex = 122;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(18, 126);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 13);
            this.label11.TabIndex = 121;
            this.label11.Text = "Limite Vac. Anual";
            // 
            // TextBoxEmployeeName
            // 
            this.TextBoxEmployeeName.Location = new System.Drawing.Point(109, 20);
            this.TextBoxEmployeeName.Name = "TextBoxEmployeeName";
            this.TextBoxEmployeeName.Size = new System.Drawing.Size(127, 21);
            this.TextBoxEmployeeName.TabIndex = 0;
            // 
            // ComboBoxEmployeeDept
            // 
            this.ComboBoxEmployeeDept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxEmployeeDept.FormattingEnabled = true;
            this.ComboBoxEmployeeDept.Location = new System.Drawing.Point(326, 89);
            this.ComboBoxEmployeeDept.Name = "ComboBoxEmployeeDept";
            this.ComboBoxEmployeeDept.Size = new System.Drawing.Size(127, 21);
            this.ComboBoxEmployeeDept.TabIndex = 120;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(7, 23);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(99, 13);
            this.label20.TabIndex = 104;
            this.label20.Text = "Nombre && Apellidos";
            // 
            // ComboBoxEmployeeManager
            // 
            this.ComboBoxEmployeeManager.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxEmployeeManager.FormattingEnabled = true;
            this.ComboBoxEmployeeManager.Location = new System.Drawing.Point(109, 89);
            this.ComboBoxEmployeeManager.Name = "ComboBoxEmployeeManager";
            this.ComboBoxEmployeeManager.Size = new System.Drawing.Size(127, 21);
            this.ComboBoxEmployeeManager.TabIndex = 119;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(281, 23);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 13);
            this.label22.TabIndex = 108;
            this.label22.Text = "Codigo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(245, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 118;
            this.label4.Text = "Departamento";
            // 
            // TextBoxEmployeeCode
            // 
            this.TextBoxEmployeeCode.Location = new System.Drawing.Point(326, 20);
            this.TextBoxEmployeeCode.Name = "TextBoxEmployeeCode";
            this.TextBoxEmployeeCode.Size = new System.Drawing.Size(127, 21);
            this.TextBoxEmployeeCode.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(38, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 117;
            this.label3.Text = "Responsable";
            // 
            // ButtonNewEmployee
            // 
            this.ButtonNewEmployee.ImageKey = "spreadsheet.ico";
            this.ButtonNewEmployee.ImageList = this.imageList2;
            this.ButtonNewEmployee.Location = new System.Drawing.Point(127, 183);
            this.ButtonNewEmployee.Name = "ButtonNewEmployee";
            this.ButtonNewEmployee.Size = new System.Drawing.Size(81, 32);
            this.ButtonNewEmployee.TabIndex = 7;
            this.ButtonNewEmployee.Text = "New";
            this.ButtonNewEmployee.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonNewEmployee.UseVisualStyleBackColor = true;
            this.ButtonNewEmployee.Click += new System.EventHandler(this.ButtonNewEmployee_Click);
            // 
            // TextBoxEmployeePhone
            // 
            this.TextBoxEmployeePhone.Location = new System.Drawing.Point(326, 56);
            this.TextBoxEmployeePhone.Name = "TextBoxEmployeePhone";
            this.TextBoxEmployeePhone.Size = new System.Drawing.Size(127, 21);
            this.TextBoxEmployeePhone.TabIndex = 115;
            // 
            // ButtonDeleteEmployee
            // 
            this.ButtonDeleteEmployee.ImageKey = "button_cancel.ico";
            this.ButtonDeleteEmployee.ImageList = this.imageList2;
            this.ButtonDeleteEmployee.Location = new System.Drawing.Point(311, 183);
            this.ButtonDeleteEmployee.Name = "ButtonDeleteEmployee";
            this.ButtonDeleteEmployee.Size = new System.Drawing.Size(82, 32);
            this.ButtonDeleteEmployee.TabIndex = 6;
            this.ButtonDeleteEmployee.Text = "Delete";
            this.ButtonDeleteEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonDeleteEmployee.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonDeleteEmployee.UseVisualStyleBackColor = true;
            this.ButtonDeleteEmployee.Click += new System.EventHandler(this.ButtonDeleteEmployee_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(256, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 116;
            this.label2.Text = "No Telefono";
            // 
            // ButtonSaveEmployee
            // 
            this.ButtonSaveEmployee.ImageKey = "button_ok.ico";
            this.ButtonSaveEmployee.ImageList = this.imageList2;
            this.ButtonSaveEmployee.Location = new System.Drawing.Point(221, 183);
            this.ButtonSaveEmployee.Name = "ButtonSaveEmployee";
            this.ButtonSaveEmployee.Size = new System.Drawing.Size(80, 32);
            this.ButtonSaveEmployee.TabIndex = 5;
            this.ButtonSaveEmployee.Text = "Save";
            this.ButtonSaveEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonSaveEmployee.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonSaveEmployee.UseVisualStyleBackColor = true;
            this.ButtonSaveEmployee.Click += new System.EventHandler(this.ButtonSaveEmployee_Click);
            // 
            // TextBoxEmployeeEmail
            // 
            this.TextBoxEmployeeEmail.Location = new System.Drawing.Point(109, 56);
            this.TextBoxEmployeeEmail.Name = "TextBoxEmployeeEmail";
            this.TextBoxEmployeeEmail.Size = new System.Drawing.Size(127, 21);
            this.TextBoxEmployeeEmail.TabIndex = 113;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(75, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 114;
            this.label1.Text = "Email";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.DataGridViewDateOfDays);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1000, 636);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Full Day/Half Day/Holiday Definition";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // DataGridViewDateOfDays
            // 
            this.DataGridViewDateOfDays.AllowUserToAddRows = false;
            this.DataGridViewDateOfDays.AllowUserToResizeRows = false;
            this.DataGridViewDateOfDays.BackgroundColor = System.Drawing.Color.White;
            this.DataGridViewDateOfDays.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewDateOfDays.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn9,
            this.Column9,
            this.Column10});
            this.DataGridViewDateOfDays.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewDateOfDays.EnableHeadersVisualStyles = false;
            this.DataGridViewDateOfDays.Location = new System.Drawing.Point(3, 3);
            this.DataGridViewDateOfDays.Name = "DataGridViewDateOfDays";
            this.DataGridViewDateOfDays.RowHeadersWidth = 20;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.DataGridViewDateOfDays.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridViewDateOfDays.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewDateOfDays.Size = new System.Drawing.Size(994, 630);
            this.DataGridViewDateOfDays.TabIndex = 104;
            this.DataGridViewDateOfDays.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewDateOfDays_CellClick);
            this.DataGridViewDateOfDays.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewDateOfDays_CellValueChanged);
            this.DataGridViewDateOfDays.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DataGridViewDateOfDays_MouseClick);
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "ID";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.HeaderText = "Date";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn9.HeaderText = "Is Full Day?";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn9.Width = 87;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Is Half Day?";
            this.Column9.Name = "Column9";
            this.Column9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Is Holiday?";
            this.Column10.Name = "Column10";
            this.Column10.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1000, 636);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = " Employee In / Out ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Khaki;
            this.groupBox4.Controls.Add(this.DataGridViewEmployeeActions);
            this.groupBox4.Controls.Add(this.panel3);
            this.groupBox4.Controls.Add(this.groupBox7);
            this.groupBox4.Controls.Add(this.panel4);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(261, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(736, 630);
            this.groupBox4.TabIndex = 113;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = " Employee Actions ";
            // 
            // DataGridViewEmployeeActions
            // 
            this.DataGridViewEmployeeActions.AllowUserToAddRows = false;
            this.DataGridViewEmployeeActions.AllowUserToOrderColumns = true;
            this.DataGridViewEmployeeActions.AllowUserToResizeRows = false;
            this.DataGridViewEmployeeActions.BackgroundColor = System.Drawing.Color.White;
            this.DataGridViewEmployeeActions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewEmployeeActions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20});
            this.DataGridViewEmployeeActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewEmployeeActions.EnableHeadersVisualStyles = false;
            this.DataGridViewEmployeeActions.Location = new System.Drawing.Point(3, 62);
            this.DataGridViewEmployeeActions.MultiSelect = false;
            this.DataGridViewEmployeeActions.Name = "DataGridViewEmployeeActions";
            this.DataGridViewEmployeeActions.ReadOnly = true;
            this.DataGridViewEmployeeActions.RowHeadersWidth = 20;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.DataGridViewEmployeeActions.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.DataGridViewEmployeeActions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewEmployeeActions.Size = new System.Drawing.Size(730, 346);
            this.DataGridViewEmployeeActions.TabIndex = 104;
            this.DataGridViewEmployeeActions.SelectionChanged += new System.EventHandler(this.DataGridViewEmployeeActions_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "Date";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 80;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn12.HeaderText = "Total";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 56;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn13.HeaderText = "1. Move";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 71;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn14.HeaderText = "2. Move";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 71;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn15.HeaderText = "3. Move";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 71;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn16.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn16.HeaderText = "4. Move";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 71;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn17.HeaderText = "5. Move";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Width = 71;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn18.HeaderText = "6. Move";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 71;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn19.HeaderText = "7. Move";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Width = 71;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn20.HeaderText = "8. Move";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.Width = 71;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Fuchsia;
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.progressBar2);
            this.panel3.Location = new System.Drawing.Point(61, 212);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(242, 41);
            this.panel3.TabIndex = 135;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(242, 18);
            this.label8.TabIndex = 9;
            this.label8.Text = "Yedek Mail İle Gönderiliyor...";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar2
            // 
            this.progressBar2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBar2.Location = new System.Drawing.Point(0, 18);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(242, 23);
            this.progressBar2.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar2.TabIndex = 8;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.DataGridViewTotalTime);
            this.groupBox7.Controls.Add(this.DataGridViewDailyActions);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox7.Location = new System.Drawing.Point(3, 408);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(730, 219);
            this.groupBox7.TabIndex = 141;
            this.groupBox7.TabStop = false;
            // 
            // DataGridViewTotalTime
            // 
            this.DataGridViewTotalTime.AllowUserToAddRows = false;
            this.DataGridViewTotalTime.AllowUserToOrderColumns = true;
            this.DataGridViewTotalTime.AllowUserToResizeRows = false;
            this.DataGridViewTotalTime.BackgroundColor = System.Drawing.Color.White;
            this.DataGridViewTotalTime.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewTotalTime.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn30});
            this.DataGridViewTotalTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.DataGridViewTotalTime.EnableHeadersVisualStyles = false;
            this.DataGridViewTotalTime.Location = new System.Drawing.Point(408, 17);
            this.DataGridViewTotalTime.MultiSelect = false;
            this.DataGridViewTotalTime.Name = "DataGridViewTotalTime";
            this.DataGridViewTotalTime.ReadOnly = true;
            this.DataGridViewTotalTime.RowHeadersWidth = 20;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            this.DataGridViewTotalTime.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.DataGridViewTotalTime.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewTotalTime.Size = new System.Drawing.Size(319, 199);
            this.DataGridViewTotalTime.TabIndex = 105;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn10.HeaderText = "Type";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.HeaderText = "Total Time";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            // 
            // DataGridViewDailyActions
            // 
            this.DataGridViewDailyActions.AllowUserToAddRows = false;
            this.DataGridViewDailyActions.AllowUserToOrderColumns = true;
            this.DataGridViewDailyActions.AllowUserToResizeRows = false;
            this.DataGridViewDailyActions.BackgroundColor = System.Drawing.Color.White;
            this.DataGridViewDailyActions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewDailyActions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn28,
            this.Column7,
            this.dataGridViewTextBoxColumn29});
            this.DataGridViewDailyActions.Dock = System.Windows.Forms.DockStyle.Left;
            this.DataGridViewDailyActions.EnableHeadersVisualStyles = false;
            this.DataGridViewDailyActions.Location = new System.Drawing.Point(3, 17);
            this.DataGridViewDailyActions.MultiSelect = false;
            this.DataGridViewDailyActions.Name = "DataGridViewDailyActions";
            this.DataGridViewDailyActions.ReadOnly = true;
            this.DataGridViewDailyActions.RowHeadersWidth = 20;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            this.DataGridViewDailyActions.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.DataGridViewDailyActions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewDailyActions.Size = new System.Drawing.Size(319, 199);
            this.DataGridViewDailyActions.TabIndex = 104;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn28.HeaderText = "Action Index";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Width = 93;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Action Time";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn29.HeaderText = "Action Name";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.CheckBoxPeriodFilter1);
            this.panel4.Controls.Add(this.CheckBoxPeriodFilter2);
            this.panel4.Controls.Add(this.GroupBoxDate);
            this.panel4.Controls.Add(this.ButtonSubmit);
            this.panel4.Controls.Add(this.GroupBoxPeriod);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 17);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(730, 45);
            this.panel4.TabIndex = 0;
            // 
            // CheckBoxPeriodFilter1
            // 
            this.CheckBoxPeriodFilter1.AutoSize = true;
            this.CheckBoxPeriodFilter1.Checked = true;
            this.CheckBoxPeriodFilter1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBoxPeriodFilter1.Location = new System.Drawing.Point(7, 6);
            this.CheckBoxPeriodFilter1.Name = "CheckBoxPeriodFilter1";
            this.CheckBoxPeriodFilter1.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxPeriodFilter1.TabIndex = 141;
            this.CheckBoxPeriodFilter1.UseVisualStyleBackColor = true;
            this.CheckBoxPeriodFilter1.CheckedChanged += new System.EventHandler(this.CheckBoxPeriodFilter1_CheckedChanged);
            // 
            // CheckBoxPeriodFilter2
            // 
            this.CheckBoxPeriodFilter2.AutoSize = true;
            this.CheckBoxPeriodFilter2.Location = new System.Drawing.Point(328, 4);
            this.CheckBoxPeriodFilter2.Name = "CheckBoxPeriodFilter2";
            this.CheckBoxPeriodFilter2.Size = new System.Drawing.Size(15, 14);
            this.CheckBoxPeriodFilter2.TabIndex = 1;
            this.CheckBoxPeriodFilter2.UseVisualStyleBackColor = true;
            this.CheckBoxPeriodFilter2.CheckedChanged += new System.EventHandler(this.CheckBoxPeriodFilter2_CheckedChanged);
            // 
            // GroupBoxDate
            // 
            this.GroupBoxDate.Controls.Add(this.DateTimePickerTo);
            this.GroupBoxDate.Controls.Add(this.label10);
            this.GroupBoxDate.Controls.Add(this.DateTimePickerFrom);
            this.GroupBoxDate.Controls.Add(this.label9);
            this.GroupBoxDate.Enabled = false;
            this.GroupBoxDate.Location = new System.Drawing.Point(328, 3);
            this.GroupBoxDate.Name = "GroupBoxDate";
            this.GroupBoxDate.Size = new System.Drawing.Size(294, 36);
            this.GroupBoxDate.TabIndex = 0;
            this.GroupBoxDate.TabStop = false;
            // 
            // DateTimePickerTo
            // 
            this.DateTimePickerTo.CustomFormat = "yyyy.MM.dd";
            this.DateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePickerTo.Location = new System.Drawing.Point(182, 12);
            this.DateTimePickerTo.Name = "DateTimePickerTo";
            this.DateTimePickerTo.Size = new System.Drawing.Size(99, 21);
            this.DateTimePickerTo.TabIndex = 138;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 137;
            this.label10.Text = "From";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DateTimePickerFrom
            // 
            this.DateTimePickerFrom.CustomFormat = "yyyy.MM.dd";
            this.DateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePickerFrom.Location = new System.Drawing.Point(52, 12);
            this.DateTimePickerFrom.Name = "DateTimePickerFrom";
            this.DateTimePickerFrom.Size = new System.Drawing.Size(99, 21);
            this.DateTimePickerFrom.TabIndex = 136;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(157, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 13);
            this.label9.TabIndex = 139;
            this.label9.Text = "To";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ButtonSubmit
            // 
            this.ButtonSubmit.ImageKey = "Search.ico";
            this.ButtonSubmit.ImageList = this.imageList2;
            this.ButtonSubmit.Location = new System.Drawing.Point(628, 9);
            this.ButtonSubmit.Name = "ButtonSubmit";
            this.ButtonSubmit.Size = new System.Drawing.Size(98, 32);
            this.ButtonSubmit.TabIndex = 140;
            this.ButtonSubmit.Text = "Submit";
            this.ButtonSubmit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonSubmit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonSubmit.UseVisualStyleBackColor = true;
            this.ButtonSubmit.Click += new System.EventHandler(this.ButtonSubmit_Click);
            // 
            // GroupBoxPeriod
            // 
            this.GroupBoxPeriod.Controls.Add(this.RadioButtonYear);
            this.GroupBoxPeriod.Controls.Add(this.RadioButtonWeek);
            this.GroupBoxPeriod.Controls.Add(this.RadioButtonMonth);
            this.GroupBoxPeriod.Controls.Add(this.RadioButtonAll);
            this.GroupBoxPeriod.Location = new System.Drawing.Point(7, 3);
            this.GroupBoxPeriod.Name = "GroupBoxPeriod";
            this.GroupBoxPeriod.Size = new System.Drawing.Size(315, 36);
            this.GroupBoxPeriod.TabIndex = 133;
            this.GroupBoxPeriod.TabStop = false;
            // 
            // RadioButtonYear
            // 
            this.RadioButtonYear.AutoSize = true;
            this.RadioButtonYear.Location = new System.Drawing.Point(69, 13);
            this.RadioButtonYear.Name = "RadioButtonYear";
            this.RadioButtonYear.Size = new System.Drawing.Size(68, 17);
            this.RadioButtonYear.TabIndex = 3;
            this.RadioButtonYear.Text = "This year";
            this.RadioButtonYear.UseVisualStyleBackColor = true;
            // 
            // RadioButtonWeek
            // 
            this.RadioButtonWeek.AutoSize = true;
            this.RadioButtonWeek.Location = new System.Drawing.Point(237, 13);
            this.RadioButtonWeek.Name = "RadioButtonWeek";
            this.RadioButtonWeek.Size = new System.Drawing.Size(74, 17);
            this.RadioButtonWeek.TabIndex = 2;
            this.RadioButtonWeek.Text = "This week";
            this.RadioButtonWeek.UseVisualStyleBackColor = true;
            // 
            // RadioButtonMonth
            // 
            this.RadioButtonMonth.AutoSize = true;
            this.RadioButtonMonth.Checked = true;
            this.RadioButtonMonth.Location = new System.Drawing.Point(153, 13);
            this.RadioButtonMonth.Name = "RadioButtonMonth";
            this.RadioButtonMonth.Size = new System.Drawing.Size(77, 17);
            this.RadioButtonMonth.TabIndex = 1;
            this.RadioButtonMonth.TabStop = true;
            this.RadioButtonMonth.Text = "This month";
            this.RadioButtonMonth.UseVisualStyleBackColor = true;
            // 
            // RadioButtonAll
            // 
            this.RadioButtonAll.AutoSize = true;
            this.RadioButtonAll.Location = new System.Drawing.Point(25, 13);
            this.RadioButtonAll.Name = "RadioButtonAll";
            this.RadioButtonAll.Size = new System.Drawing.Size(36, 17);
            this.RadioButtonAll.TabIndex = 0;
            this.RadioButtonAll.Text = "All";
            this.RadioButtonAll.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.PowderBlue;
            this.groupBox9.Controls.Add(this.DataGridViewEmployeeList);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox9.Location = new System.Drawing.Point(3, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(258, 630);
            this.groupBox9.TabIndex = 112;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = " Employees ";
            // 
            // DataGridViewEmployeeList
            // 
            this.DataGridViewEmployeeList.AllowUserToAddRows = false;
            this.DataGridViewEmployeeList.AllowUserToOrderColumns = true;
            this.DataGridViewEmployeeList.AllowUserToResizeRows = false;
            this.DataGridViewEmployeeList.BackgroundColor = System.Drawing.Color.White;
            this.DataGridViewEmployeeList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewEmployeeList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23});
            this.DataGridViewEmployeeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewEmployeeList.EnableHeadersVisualStyles = false;
            this.DataGridViewEmployeeList.Location = new System.Drawing.Point(3, 17);
            this.DataGridViewEmployeeList.MultiSelect = false;
            this.DataGridViewEmployeeList.Name = "DataGridViewEmployeeList";
            this.DataGridViewEmployeeList.ReadOnly = true;
            this.DataGridViewEmployeeList.RowHeadersWidth = 20;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            this.DataGridViewEmployeeList.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.DataGridViewEmployeeList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewEmployeeList.Size = new System.Drawing.Size(252, 610);
            this.DataGridViewEmployeeList.TabIndex = 103;
            this.DataGridViewEmployeeList.SelectionChanged += new System.EventHandler(this.DataGridViewEmployeeList_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.HeaderText = "ID";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Visible = false;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn22.HeaderText = "Name && Surname";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn23.HeaderText = "Status";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Width = 63;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox10);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1000, 636);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Action Flows";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox10.Controls.Add(this.groupBox6);
            this.groupBox10.Controls.Add(this.groupBox8);
            this.groupBox10.Location = new System.Drawing.Point(214, 141);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(574, 355);
            this.groupBox10.TabIndex = 108;
            this.groupBox10.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.DataGridViewActions);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox6.Location = new System.Drawing.Point(3, 17);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(279, 335);
            this.groupBox6.TabIndex = 106;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = " First Actions ";
            // 
            // DataGridViewActions
            // 
            this.DataGridViewActions.AllowUserToAddRows = false;
            this.DataGridViewActions.AllowUserToOrderColumns = true;
            this.DataGridViewActions.AllowUserToResizeRows = false;
            this.DataGridViewActions.BackgroundColor = System.Drawing.Color.White;
            this.DataGridViewActions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewActions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25});
            this.DataGridViewActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewActions.EnableHeadersVisualStyles = false;
            this.DataGridViewActions.Location = new System.Drawing.Point(3, 17);
            this.DataGridViewActions.MultiSelect = false;
            this.DataGridViewActions.Name = "DataGridViewActions";
            this.DataGridViewActions.ReadOnly = true;
            this.DataGridViewActions.RowHeadersWidth = 20;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            this.DataGridViewActions.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.DataGridViewActions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewActions.Size = new System.Drawing.Size(273, 315);
            this.DataGridViewActions.TabIndex = 104;
            this.DataGridViewActions.SelectionChanged += new System.EventHandler(this.DataGridViewActions_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "ID";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Visible = false;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn25.HeaderText = "Action Name";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.DataGridViewPossibleActions);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox8.Location = new System.Drawing.Point(305, 17);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(266, 335);
            this.groupBox8.TabIndex = 107;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = " Possible Next Actions ";
            // 
            // DataGridViewPossibleActions
            // 
            this.DataGridViewPossibleActions.AllowUserToAddRows = false;
            this.DataGridViewPossibleActions.AllowUserToOrderColumns = true;
            this.DataGridViewPossibleActions.AllowUserToResizeRows = false;
            this.DataGridViewPossibleActions.BackgroundColor = System.Drawing.Color.White;
            this.DataGridViewPossibleActions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewPossibleActions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.Column6,
            this.dataGridViewTextBoxColumn27});
            this.DataGridViewPossibleActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridViewPossibleActions.EnableHeadersVisualStyles = false;
            this.DataGridViewPossibleActions.Location = new System.Drawing.Point(3, 17);
            this.DataGridViewPossibleActions.MultiSelect = false;
            this.DataGridViewPossibleActions.Name = "DataGridViewPossibleActions";
            this.DataGridViewPossibleActions.RowHeadersWidth = 20;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            this.DataGridViewPossibleActions.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.DataGridViewPossibleActions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewPossibleActions.Size = new System.Drawing.Size(260, 315);
            this.DataGridViewPossibleActions.TabIndex = 105;
            this.DataGridViewPossibleActions.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewPossibleActions_CellClick);
            this.DataGridViewPossibleActions.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewPossibleActions_CellValueChanged);
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "ID";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.Visible = false;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column6.HeaderText = "";
            this.Column6.Name = "Column6";
            this.Column6.Width = 5;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn27.HeaderText = "Action Name";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // DayTypeContextMenuStrip
            // 
            this.DayTypeContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fullDayToolStripMenuItem,
            this.halfDayToolStripMenuItem,
            this.holidayToolStripMenuItem});
            this.DayTypeContextMenuStrip.Name = "DayTypeContextMenuStrip";
            this.DayTypeContextMenuStrip.Size = new System.Drawing.Size(120, 70);
            // 
            // fullDayToolStripMenuItem
            // 
            this.fullDayToolStripMenuItem.Name = "fullDayToolStripMenuItem";
            this.fullDayToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.fullDayToolStripMenuItem.Text = "Full Day";
            this.fullDayToolStripMenuItem.Click += new System.EventHandler(this.fullDayToolStripMenuItem_Click);
            // 
            // halfDayToolStripMenuItem
            // 
            this.halfDayToolStripMenuItem.Name = "halfDayToolStripMenuItem";
            this.halfDayToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.halfDayToolStripMenuItem.Text = "Half Day";
            this.halfDayToolStripMenuItem.Click += new System.EventHandler(this.halfDayToolStripMenuItem_Click);
            // 
            // holidayToolStripMenuItem
            // 
            this.holidayToolStripMenuItem.Name = "holidayToolStripMenuItem";
            this.holidayToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.holidayToolStripMenuItem.Text = "Holiday";
            this.holidayToolStripMenuItem.Click += new System.EventHandler(this.holidayToolStripMenuItem_Click);
            // 
            // FormAdminMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 662);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Name = "FormAdminMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin Panel";
            this.Load += new System.EventHandler(this.FormAdminMain_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewEmployee)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.GroupBoxEmployeeVacations.ResumeLayout(false);
            this.GroupBoxEmployeeVacations.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewEmployeeVacations)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownVacLimit)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDateOfDays)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewEmployeeActions)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewTotalTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewDailyActions)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.GroupBoxDate.ResumeLayout(false);
            this.GroupBoxDate.PerformLayout();
            this.GroupBoxPeriod.ResumeLayout(false);
            this.GroupBoxPeriod.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewEmployeeList)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewActions)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewPossibleActions)).EndInit();
            this.DayTypeContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ComboBox ComboBoxEmployeeDept;
        private System.Windows.Forms.ComboBox ComboBoxEmployeeManager;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxEmployeePhone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxEmployeeEmail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonSaveEmployee;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Button ButtonDeleteEmployee;
        private System.Windows.Forms.Button ButtonNewEmployee;
        private System.Windows.Forms.TextBox TextBoxEmployeeCode;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox TextBoxEmployeeName;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView DataGridViewDateOfDays;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column10;
        private System.Windows.Forms.ContextMenuStrip DayTypeContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fullDayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem halfDayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem holidayToolStripMenuItem;
        private System.Windows.Forms.DataGridView DataGridViewEmployee;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView DataGridViewEmployeeActions;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DateTimePicker DateTimePickerTo;
        private System.Windows.Forms.Button ButtonSubmit;
        private System.Windows.Forms.GroupBox GroupBoxPeriod;
        private System.Windows.Forms.RadioButton RadioButtonYear;
        private System.Windows.Forms.RadioButton RadioButtonWeek;
        private System.Windows.Forms.RadioButton RadioButtonMonth;
        private System.Windows.Forms.RadioButton RadioButtonAll;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker DateTimePickerFrom;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView DataGridViewEmployeeList;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox GroupBoxEmployeeVacations;
        private System.Windows.Forms.DataGridView DataGridViewEmployeeVacations;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker DateTimePickerVacationTo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker DateTimePickerVacationFrom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ButtonNewVacation;
        private System.Windows.Forms.Button ButtonDeleteVacation;
        private System.Windows.Forms.Button ButtonSaveVacation;
        private System.Windows.Forms.GroupBox GroupBoxDate;
        private System.Windows.Forms.CheckBox CheckBoxPeriodFilter2;
        private System.Windows.Forms.CheckBox CheckBoxPeriodFilter1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView DataGridViewPossibleActions;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView DataGridViewActions;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridView DataGridViewDailyActions;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridView DataGridViewTotalTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.Label LabelUsedDayCount;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.NumericUpDown NumericUpDownVacLimit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label LabelRemDayCount;
        private System.Windows.Forms.Label label14;
    }
}

