﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimeControllerAdminApp
{
    public partial class FormAdminMain : Form
    {
        DataSet _EmployeeDS = null;
        DataSet _DepartmentDS = null;
        DataSet _VacationDS = null;
        DataSet _ActionDS = null;
        DataSet _ActionFlowDS = null;
        Boolean _LoadFinished = false;
        DataSet _EmployeeActionsDS = null;
        DataTable _EmployeeActionsDT = null;

        //TODO:This values must be set dynamically not statically
        int _InMorning = 1;
        int _InSpecialCustomerVendor = 2;
        int _InSpecialPersonal = 3;
        int _InSpecialGenDocto = 4;
        int _InSpecialSpeDoctor = 5;
        int _InLunch = 6;
        int _OutLunch = 7;
        int _OutSpecialCustomerVendor = 8;
        int _OutSpecialPersonal = 9;
        int _OutSpecialGenDoctor = 10;
        int _OutSpecialSpeDoctor = 11;
        int _OutExit = 12;


        public FormAdminMain()
        {
            InitializeComponent();
        }

        private void FormAdminMain_Load(object sender, EventArgs e)
        {
            LoadEmployees();
            LoadDepartments();
            ButtonNewEmployee_Click(null, null);
            _LoadFinished = true;
        }

        private void LoadEmployees()
        {
            try
            {
                String sql = @"SELECT e.*, d.DepartmentName, s.StatusName, e2.NameSurname as ManagerName 
                               from Employees e LEFT OUTER JOIN Status s ON e.StatusID = s.StatusID 
                               LEFT OUTER JOIN Department d ON e.DepartmentID = d.DepartmentID
                               LEFT OUTER JOIN Employees e2 ON e.ManagerID = e2.EmployeeID and e2.IsDeleted = 0
                               WHERE e.IsDeleted = 0  ORDER BY e.NameSurname";
                _EmployeeDS = DB.Sorgula(sql, "");
                DataGridViewEmployee.Rows.Clear();
                if (_EmployeeDS != null && _EmployeeDS.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in _EmployeeDS.Tables[0].Rows)
                    {
                        String id = "";
                        String name = "";
                        String status = "";
                        String code = "";
                        String email = "";
                        String manager = "";
                        String department = "";
                        String phone = "";

                        id = dr["EmployeeID"].ToString();
                        name = dr["NameSurname"].ToString();
                        code = dr["Code"].ToString();
                        if (!dr["StatusName"].Equals(DBNull.Value))
                            status = dr["StatusName"].ToString();

                        if (!dr["Email"].Equals(DBNull.Value))
                            email = dr["Email"].ToString();
                        if (!dr["PhoneNumber"].Equals(DBNull.Value))
                            phone = dr["PhoneNumber"].ToString();
                        if (!dr["ManagerName"].Equals(DBNull.Value))
                            manager = dr["ManagerName"].ToString();
                        if (!dr["DepartmentName"].Equals(DBNull.Value))
                            department = dr["DepartmentName"].ToString();
                        DataGridViewEmployee.Rows.Add(id, name, status, code, email, phone, manager, department);
                    }
                    if (DataGridViewEmployee.SelectedRows.Count > 0)
                    {
                        DataGridViewEmployee.SelectedRows[0].Selected = false;
                    }
                    ComboBoxEmployeeManager.DisplayMember = "NameSurname";
                    ComboBoxEmployeeManager.ValueMember = "EmployeeID";
                    ComboBoxEmployeeManager.DataSource = _EmployeeDS.Tables[0];
                }
                

                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception occured when loading Employees!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void LoadDepartments()
        {
            try
            {
                String sql = "SELECT * from dbo.Department ORDER BY DepartmentName";
                _DepartmentDS = DB.Sorgula(sql, "");
                
                ComboBoxEmployeeDept.DisplayMember = "DepartmentName";
                ComboBoxEmployeeDept.ValueMember = "DepartmentID";
                ComboBoxEmployeeDept.DataSource = _DepartmentDS.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception occured when loading Departments!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void LoadActions()
        {
            try
            {
                String sql = "SELECT * from dbo.Actions";
                _ActionDS = DB.Sorgula(sql, "");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception occured when loading Actions!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void LoadActionFlows()
        {
            try
            {
                String sql = "SELECT * from dbo.ActionFlow";
                _ActionFlowDS = DB.Sorgula(sql, "");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception occured when loading ActionFlow!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void LoadEmptyPossibleActions()
        {
            DataGridViewPossibleActions.Rows.Clear();
            foreach (DataRow dr in _ActionDS.Tables[0].Rows)
            {
                DataGridViewPossibleActions.Rows.Add(dr["ActionID"].ToString(), false, dr["ActionName"].ToString());
            }
            if (DataGridViewPossibleActions.SelectedRows.Count > 0)
                DataGridViewPossibleActions.SelectedRows[0].Selected = false;
        }

        private void LoadPossibleActions(String actionId)
        {
            try
            {
                String sql = "SELECT * from dbo.ActionFlow WHERE FirstActionID = " + actionId;
                DataSet ds = DB.Sorgula(sql, "");
                if (ds == null) throw new Exception("");
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    foreach (DataGridViewRow dgvr in DataGridViewPossibleActions.Rows)
                    {
                        if (dgvr.Cells[0].Value.ToString().Equals(dr["NextActionID"].ToString()))
                        {
                            DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)dgvr.Cells[1];
                            cell.Value = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception occured when loading Actions!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
        }


        private void DataGridViewEmployee_SelectionChanged(object sender, EventArgs e)
        {
            if (!_LoadFinished) return;
            ComboBoxEmployeeDept.SelectedIndex = -1;
            ComboBoxEmployeeManager.SelectedIndex = -1;
            if (DataGridViewEmployee.SelectedRows.Count == 0)
                return;

            string EmployeeID = DataGridViewEmployee.SelectedRows[0].Cells[0].Value.ToString();
            foreach (DataRow dr in _EmployeeDS.Tables[0].Rows)
            {
                if (dr["EmployeeID"].ToString().Equals(EmployeeID))
                {
                    TextBoxEmployeeName.Text = dr["NameSurname"].ToString();
                    TextBoxEmployeeCode.Text = dr["Code"].ToString();
                    TextBoxEmployeeEmail.Text = dr["Email"].ToString();
                    TextBoxEmployeePhone.Text = dr["PhoneNumber"].ToString();
                    if (dr["AnnualVacationLimit"] != null)
                        NumericUpDownVacLimit.Value = Convert.ToInt32(dr["AnnualVacationLimit"]);
                    else
                        NumericUpDownVacLimit.Value = 0;

                    if (dr["DepartmentID"] != DBNull.Value && _DepartmentDS != null)
                    {
                        foreach (DataRow drd in _DepartmentDS.Tables[0].Rows)
                        {
                            if (drd["DepartmentID"].ToString().Equals(dr["DepartmentID"].ToString()))
                            {
                                ComboBoxEmployeeDept.SelectedValue = Convert.ToInt32(drd["DepartmentID"].ToString());
                                break;
                            }
                        }
                    }
                    if (dr["ManagerID"] != DBNull.Value)
                    {
                        foreach (DataRow dre in _EmployeeDS.Tables[0].Rows)
                        {
                            if (dre["EmployeeID"].ToString().Equals(dr["ManagerID"].ToString()))
                            {
                                ComboBoxEmployeeManager.SelectedValue = Convert.ToInt32(dre["EmployeeID"].ToString());
                                break;
                            }
                        }
                    }
                    LoadVacations(EmployeeID);
                    break;
                }
            }
        }

        private void ButtonNewEmployee_Click(object sender, EventArgs e)
        {
            TextBoxEmployeeName.Text = "";
            TextBoxEmployeeCode.Text = "";
            TextBoxEmployeeEmail.Text = "";
            TextBoxEmployeePhone.Text = "";
            NumericUpDownVacLimit.Value = 0;
            ComboBoxEmployeeDept.SelectedIndex = -1;
            ComboBoxEmployeeManager.SelectedIndex = -1;
            if (DataGridViewEmployee.SelectedRows.Count > 0)
                DataGridViewEmployee.SelectedRows[0].Selected = false;
        }

        private void ButtonSaveEmployee_Click(object sender, EventArgs e)
        {
            TextBoxEmployeeName.Text = Utility.InputFilter(TextBoxEmployeeName.Text);
            TextBoxEmployeeCode.Text = Utility.InputFilter(TextBoxEmployeeCode.Text);
            TextBoxEmployeeEmail.Text = Utility.InputFilter(TextBoxEmployeeEmail.Text);
            TextBoxEmployeePhone.Text = Utility.InputFilter(TextBoxEmployeePhone.Text);

            if (TextBoxEmployeeName.Text == "" || TextBoxEmployeeCode.Text == "")
            {
                MessageBox.Show("Employee name&surname and code must be filled!", "Save Employee", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (DataGridViewEmployee.SelectedRows.Count == 0) //insert
            {
                string sorgu = "INSERT INTO Employees(Code, NameSurname, Email, PhoneNumber, AnnualVacationLimit";
                string values = " VALUES('" + TextBoxEmployeeCode.Text + "', " +
                                "'" + TextBoxEmployeeName.Text + "', " +
                                "'" + TextBoxEmployeeEmail.Text + "', " +
                                "'" + TextBoxEmployeePhone.Text + "', "
                                + NumericUpDownVacLimit.Value.ToString();
                if(ComboBoxEmployeeDept.SelectedIndex > 0 && Convert.ToInt32(ComboBoxEmployeeDept.SelectedValue) > 0)
                {
                    sorgu += ", DepartmentID";
                    values += ", " + Convert.ToInt32(ComboBoxEmployeeDept.SelectedValue);
                }
                if(ComboBoxEmployeeManager.SelectedIndex > 0 && Convert.ToInt32(ComboBoxEmployeeManager.SelectedValue) > 0)
                {
                    sorgu += ", ManagerID";
                    values += ", " + Convert.ToInt32(ComboBoxEmployeeManager.SelectedValue);
                }
                sorgu = sorgu + ")" + values + ")";
                try
                {
                    if (DB.Uygula(sorgu, "ButtonSaveEmployee_Click"))
                    {
                        MessageBox.Show("Employee saved.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadEmployees();
                        DataSet ds = DB.Sorgula("SELECT MAX(EmployeeId) FROM Employees", "ButtonSaveEmployee_Click");
                        string id = ds.Tables[0].Rows[0][0].ToString();
                        foreach (DataGridViewRow dgrv in DataGridViewEmployee.Rows)
                        {
                            if (dgrv.Cells[0].Value.ToString() == id)
                            {
                                dgrv.Selected = true;
                                DataGridViewEmployee.FirstDisplayedScrollingRowIndex = dgrv.Index;
                                return;
                            }
                        }
                    }
                    else
                        throw new Exception("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Exception occured while saving Employee!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine(ex.StackTrace);
                }
            }
            else//update
            {
                string EmployeeID = DataGridViewEmployee.SelectedRows[0].Cells[0].Value.ToString();
                string sorgu = "UPDATE Employees SET NameSurname = '" + TextBoxEmployeeName.Text +
                    "', Code = '" + TextBoxEmployeeCode.Text +
                    "', Email = '" + TextBoxEmployeeEmail.Text +
                    "', AnnualVacationLimit = " + NumericUpDownVacLimit.Value.ToString() +
                    ", PhoneNumber = '" + TextBoxEmployeePhone.Text + "'";
                if (ComboBoxEmployeeDept.SelectedIndex != -1 && Convert.ToInt32(ComboBoxEmployeeDept.SelectedValue) > 0)
                {
                    sorgu += ", DepartmentID = " + Convert.ToInt32(ComboBoxEmployeeDept.SelectedValue);
                }
                else
                {
                    sorgu += ", DepartmentID = NULL";
                }
                if (ComboBoxEmployeeManager.SelectedIndex != -1 && Convert.ToInt32(ComboBoxEmployeeManager.SelectedValue) > 0)
                {
                    sorgu += ", ManagerID = " + Convert.ToInt32(ComboBoxEmployeeManager.SelectedValue);
                }
                else
                {
                    sorgu += ", ManagerID = NULL";
                }
                sorgu += " WHERE EmployeeID = " + EmployeeID;
                try
                {
                    if (DB.Uygula(sorgu, "ButtonSaveEmployee_Click"))
                    {
                        MessageBox.Show("Employee updated.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadEmployees();
                        foreach (DataGridViewRow dgrv in DataGridViewEmployee.Rows)
                        {
                            if (dgrv.Cells[0].Value.ToString() == EmployeeID)
                            {
                                dgrv.Selected = true;
                                DataGridViewEmployee.FirstDisplayedScrollingRowIndex = dgrv.Index;
                                return;
                            }
                        }
                    }
                    else throw new Exception("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error occured while updating Employee!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine(ex.StackTrace);
                }
            }
        }

        private void ButtonDeleteEmployee_Click(object sender, EventArgs e)
        {
            if (DataGridViewEmployee.SelectedRows.Count == 0)
            {
                MessageBox.Show("You must select an Employee", "Delete Employee", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            DialogResult result = MessageBox.Show("Are you sure that you want to delete the Employee?", "Delete Employee", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result != DialogResult.Yes) return;

            string EmployeeId = DataGridViewEmployee.SelectedRows[0].Cells[0].Value.ToString();
            try
            {
                string sorgu = "UPDATE Employees SET IsDeleted = 1 WHERE EmployeeID = " + EmployeeId;
                if (DB.Uygula(sorgu, "ButtonDeleteEmployee_Click"))
                {
                    MessageBox.Show("Employee deleted.", "Delete Employee", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadEmployees();
                    ButtonNewEmployee_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured while updating Employee!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 1)
                FillDateGrid();
            else if (tabControl1.SelectedIndex == 2)
            {
                LoadActions();
                if (_EmployeeDS != null && _EmployeeDS.Tables[0].Rows.Count > 0)
                {
                    DataGridViewEmployeeList.Rows.Clear();
                    foreach (DataRow dr in _EmployeeDS.Tables[0].Rows)
                    {
                        String id = "";
                        String name = "";
                        String status = "";

                        id = dr["EmployeeID"].ToString();
                        name = dr["NameSurname"].ToString();

                        if (!dr["StatusName"].Equals(DBNull.Value))
                            status = dr["StatusName"].ToString();

                        DataGridViewEmployeeList.Rows.Add(id, name, status);
                    }
                    if (DataGridViewEmployeeList.SelectedRows.Count > 0)
                    {
                        DataGridViewEmployeeList.SelectedRows[0].Selected = false;
                    }
                }
            }
            else if (tabControl1.SelectedIndex == 3)
            {
                DataGridViewActions.Rows.Clear();
                LoadActions();
                foreach (DataRow dr in _ActionDS.Tables[0].Rows)
                {
                    DataGridViewActions.Rows.Add(dr["ActionID"].ToString(), dr["ActionName"].ToString());
                }
                if (DataGridViewActions.SelectedRows.Count > 0)
                    DataGridViewActions.SelectedRows[0].Selected = false;
                DataGridViewPossibleActions.Rows.Clear();
            }
        }

        private void FillDateGrid()
        {
            DataGridViewDateOfDays.Rows.Clear();
            DateTime firstDayOfTheThisMonth = Utility.GetFirstDayOfThisMonth();
            DateTime firstDayOfThePreviousMonth = firstDayOfTheThisMonth.AddMonths(-1);
            DateTime firstDayOfTheNextMonth = firstDayOfTheThisMonth.AddMonths(1);

            for (DateTime date = firstDayOfThePreviousMonth; date < firstDayOfTheNextMonth.AddMonths(1); date = date.AddDays(1))
            {
                CheckIfDateExistsInTable(date);
            }
            try
            {
                String sql = "SELECT * from DateOfDays ORDER BY DateOfDay";
                DataSet ds = DB.Sorgula(sql, "FillDateGrid");
                if (ds == null)
                    throw new Exception("");
                foreach(DataRow dr in ds.Tables[0].Rows)
                {
                    DateTime date = Convert.ToDateTime(dr["DateOfDay"].ToString());
                    DataGridViewDateOfDays.Rows.Add(dr["DateOfDayID"].ToString(), date.ToLongDateString(), Convert.ToBoolean(dr["IsFullDay"].ToString()), Convert.ToBoolean(dr["IsHalfDay"].ToString()), Convert.ToBoolean(dr["IsHoliday"].ToString()));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured while filling the calendar!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private bool CheckIfDateExistsInTable(DateTime date)
        {
            try
            {
                string sorgu = "SELECT * FROM DateOfDays where YEAR(DateOfDay) = " + date.Year + " AND MONTH(DateOfDay) = " + date.Month + " AND DAY(DateOfDay) = " + date.Day;
                DataSet ds = DB.Sorgula(sorgu, "CheckIfDateExistsInTable");
                if (ds != null && ds.Tables[0].Rows.Count == 0)
                {
                    string sql = "INSERT INTO DateOfDays(DateOfDay, IsFullDay, IsHalfDay, IsHoliday) VALUES(";
                    sql += "'" + date.Year + "-" + date.Month + "-" + date.Day + "', 0, 0, 0)";
                    if (!DB.Uygula(sql, "CheckIfDateExistsInTable"))
                    {
                        throw new Exception("");
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured while checking dates!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
            return false;
        }

        private void DataGridViewDateOfDays_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DayTypeContextMenuStrip.Show(DataGridViewDateOfDays, e.Location);
            }
        }

        private void DataGridViewDateOfDays_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex == -1 || e.ColumnIndex == 0 || e.ColumnIndex == 1) return;

            DataGridViewCheckBoxCell curCell = (DataGridViewCheckBoxCell)DataGridViewDateOfDays.Rows[e.RowIndex].Cells[e.ColumnIndex];
            ((DataGridViewCheckBoxCell)curCell.OwningRow.Cells[2]).Value = false;
            ((DataGridViewCheckBoxCell)curCell.OwningRow.Cells[3]).Value = false;
            ((DataGridViewCheckBoxCell)curCell.OwningRow.Cells[4]).Value = false;
        }

        private void DataGridViewDateOfDays_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex == -1) return;
            DataGridViewCheckBoxCell curCell = (DataGridViewCheckBoxCell)DataGridViewDateOfDays.Rows[e.RowIndex].Cells[e.ColumnIndex];
            String dayId = curCell.OwningRow.Cells[0].Value.ToString();
            int val = Convert.ToInt32(curCell.Value);
            try
            {
                String columnName = "";
                if (e.ColumnIndex == 2) columnName = "IsFullDay";
                else if (e.ColumnIndex == 3) columnName = "IsHalfDay";
                else if (e.ColumnIndex == 4) columnName = "IsHoliday";
                String sql = "UPDATE DATEOFDAYS SET " + columnName + " = " + val.ToString() + " WHERE DateOfDayID = " + dayId;
                if (!DB.Uygula(sql, ""))
                {
                    throw new Exception("DataGridViewDateOfDays_CellValueChanged");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured while updating the values!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void fullDayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetDayTypeOfSelectedRows(true, false, false);
        }

        private void halfDayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetDayTypeOfSelectedRows(false, true, false);
        }

        private void holidayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetDayTypeOfSelectedRows(false, false, true);
        }

        private void SetDayTypeOfSelectedRows(bool isFullDay, bool isHalfDay, bool isHoliday)
        {
            if (DataGridViewDateOfDays.SelectedRows.Count == 0) return;

            for (int i = 0; i < DataGridViewDateOfDays.SelectedRows.Count; i++)
            {
                DataGridViewDateOfDays.SelectedRows[i].Cells[2].Value = isFullDay;
                DataGridViewDateOfDays.SelectedRows[i].Cells[3].Value = isHalfDay;
                DataGridViewDateOfDays.SelectedRows[i].Cells[4].Value = isHoliday;
            }
        }

        private void ButtonNewVacation_Click(object sender, EventArgs e)
        {
            if (DataGridViewEmployeeVacations.SelectedRows.Count > 0)
                DataGridViewEmployeeVacations.SelectedRows[0].Selected = false;
            DateTimePickerVacationFrom.Value = DateTime.Now;
            DateTimePickerVacationTo.Value = DateTime.Now;
        }

        private void ButtonSaveVacation_Click(object sender, EventArgs e)
        {
            if (DataGridViewEmployee.SelectedRows.Count == 0)
            {
                MessageBox.Show("You must select an employee first!", "Save Vacation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DateTime fromDate = DateTimePickerVacationFrom.Value;
            DateTime toDate = DateTimePickerVacationTo.Value;
            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 0, 0, 0);
            if (fromDate.CompareTo(toDate) >= 0)
            {
                MessageBox.Show("'From Date' must be less than 'To Date' at least one day", "Save Vacation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //select holidays in current year
            String ss = "SELECT * FROM DateOfDays where YEAR(DateOfDay) = " + DateTime.Now.Year + " AND IsHoliday = 1";
            ArrayList availableDays = new ArrayList();
            try
            {
                DataSet ds = DB.Sorgula(ss, "ButtonSaveVacation_Click");
                if (ds == null) throw new Exception("");
                
                for (DateTime d = fromDate; d < toDate; d = d.AddDays(1))
                {
                    //check if the vacationdate is holiday
                    DataRow[] drs = ds.Tables[0].Select("DateOfDay = '" + String.Format("{0:yyyy-MM-dd 00:00:00}", d) + "'");
                    if (drs.Length == 0)
                        availableDays.Add(d);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception occured while saving vacation!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }

            if (Convert.ToInt32(LabelRemDayCount.Text) < availableDays.Count)
            {
                MessageBox.Show("Annual Vacation Date Limit Exceeded!", "Save Vacation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            String EmployeeID = DataGridViewEmployee.SelectedRows[0].Cells[0].Value.ToString();
            if (DataGridViewEmployeeVacations.SelectedRows.Count == 0)//insertğüğ
            {
                ArrayList sqlArray = new ArrayList();
                foreach(DateTime date in availableDays)
                {
                    String sql = "INSERT INTO EmployeeVacations(EmployeeID, VacationDate) VALUES (";
                    sql += EmployeeID + ", ";
                    sql += "'" + date.Year + "-" + date.Month + "-" + date.Day + "');";
                    sqlArray.Add(sql);
                }
                try
                {
                    if (DB.UygulaTrans(sqlArray.ToArray(), "ButtonSaveVacation_Click"))
                    {
                        MessageBox.Show("Employee vacation saved.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadVacations(EmployeeID);
                        /*
                        DataSet ds = DB.Sorgula("SELECT MAX(EmployeeVacationID) FROM EmployeeVacations", "ButtonSaveVacation_Click");
                        string id = ds.Tables[0].Rows[0][0].ToString();
                        foreach (DataGridViewRow dgrv in DataGridViewEmployeeVacations.Rows)
                        {
                            if (dgrv.Cells[0].Value.ToString() == id)
                            {
                                dgrv.Selected = true;
                                DataGridViewEmployeeVacations.FirstDisplayedScrollingRowIndex = dgrv.Index;
                                return;
                            }
                        }*/
                    }
                    else
                        throw new Exception("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Exception occured while saving vacation!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine(ex.StackTrace);
                }
            }
            else//update
            {
                try
                {
                    String vacationID = DataGridViewEmployeeVacations.SelectedRows[0].Cells[0].Value.ToString();
                        
                    String sorgu = "UPDATE EmployeeVacations SET ";
                    sorgu += "VacationDateFrom = '" + fromDate.Year + "-" + fromDate.Month + "-" + fromDate.Day + "', ";
                    sorgu += "VacationDateTo = '" + toDate.Year + "-" + toDate.Month + "-" + toDate.Day + "' ";
                    sorgu += "WHERE EmployeeVacationID = " + vacationID;
                    if (DB.Uygula(sorgu, "ButtonSaveVacation_Click"))
                    {
                        MessageBox.Show("Vacation updated.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadVacations(EmployeeID);
                        foreach (DataGridViewRow dgrv in DataGridViewEmployeeVacations.Rows)
                        {
                            if (dgrv.Cells[0].Value.ToString() == vacationID)
                            {
                                dgrv.Selected = true;
                                DataGridViewEmployeeVacations.FirstDisplayedScrollingRowIndex = dgrv.Index;
                                return;
                            }
                        }
                    }
                    else throw new Exception("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error occured while updating vacation!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Console.WriteLine(ex.StackTrace);
                }
            }
        }

        private void LoadVacations(String EmployeeID)
        {
            DataGridViewEmployeeVacations.Rows.Clear();
            try
            {
                int currentYear = DateTime.Now.Year;
                String firstDatOfTheYear = currentYear + "-01-01 00:00:00";
                String lastDatOfTheYear = currentYear + "-12-31 23:59:59";
                String sql = "SELECT * from dbo.EmployeeVacations WHERE EmployeeID = " + EmployeeID + " AND VacationDate BETWEEN '" + firstDatOfTheYear + "' AND '" + lastDatOfTheYear + "' ORDER BY VacationDate desc";
                _VacationDS = DB.Sorgula(sql, "");

                if (_VacationDS == null) throw new Exception("");

                if (_VacationDS.Tables[0].Rows.Count == 1)
                {
                    DataRow dr = _VacationDS.Tables[0].Rows[0];
                    DataGridViewEmployeeVacations.Rows.Add(
                        dr["EmployeeVacationID"].ToString(), 
                        String.Format("{0:yyyy.MM.dd}", Convert.ToDateTime(dr["VacationDate"].ToString())), 
                        String.Format("{0:yyyy.MM.dd}", Convert.ToDateTime(dr["VacationDate"].ToString()).AddDays(1)), 1);
                }
                else if (_VacationDS.Tables[0].Rows.Count > 1)
                {
                    DateTime fromDate = DateTime.MinValue;
                    DateTime toDate = DateTime.MinValue;
                    //DateTime date = DateTime.MinValue;
                    foreach (DataRow dr in _VacationDS.Tables[0].Rows)
                    {
                        if (toDate.Equals(DateTime.MinValue))
                        {
                            toDate = Convert.ToDateTime(dr["VacationDate"].ToString()).AddDays(1);
                            fromDate = Convert.ToDateTime(dr["VacationDate"].ToString());
                            continue;
                        }

                        DateTime tempDate = Convert.ToDateTime(dr["VacationDate"].ToString());
                        if (!tempDate.Date.Equals(fromDate.AddDays(-1).Date))
                        {
                            DataGridViewEmployeeVacations.Rows.Add(dr["EmployeeVacationID"].ToString(), String.Format("{0:yyyy.MM.dd}", fromDate), String.Format("{0:yyyy.MM.dd}", toDate), toDate.Subtract(fromDate).Days);
                            toDate = tempDate.AddDays(1);
                            fromDate = tempDate;
                        }else
                            fromDate = fromDate.AddDays(-1); 
                    }
                    if (!fromDate.Equals(DateTime.MinValue))
                    {
                        DataGridViewEmployeeVacations.Rows.Add(_VacationDS.Tables[0].Rows[_VacationDS.Tables[0].Rows.Count-1]["EmployeeVacationID"].ToString(),
                            String.Format("{0:yyyy.MM.dd}", fromDate), 
                            String.Format("{0:yyyy.MM.dd}", toDate), 
                            toDate.Subtract(fromDate).Days);
                    }
                }
                CalculateRemainingVacations();
                ButtonNewVacation_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception occured when loading employee vacations!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void CalculateRemainingVacations()
        {
            int usedVacations = 0;
            int remainingVacations = 0;
            int totalVacations = 0;
            LabelUsedDayCount.Text = usedVacations.ToString();
            LabelRemDayCount.Text = remainingVacations.ToString();
            if (DataGridViewEmployee.SelectedRows.Count == 0)
                return;

            if (DataGridViewEmployeeVacations.Rows.Count > 0)
            {
                foreach (DataGridViewRow dgvr in DataGridViewEmployeeVacations.Rows)
                {
                    usedVacations += Convert.ToInt32(dgvr.Cells[3].Value);
                }
                LabelUsedDayCount.Text = usedVacations.ToString();
            }
            String selectedEmployee = DataGridViewEmployee.SelectedRows[0].Cells[0].Value.ToString();
            //We must take total vacation day count from dataset, not from numericupdown value!
            //Because it can be changed manually.
            foreach (DataRow dr in _EmployeeDS.Tables[0].Rows)
            {
                if (selectedEmployee.Equals(dr["EmployeeID"].ToString()))
                {
                    totalVacations = Convert.ToInt32(dr["AnnualVacationLimit"].ToString());
                    break;
                }
            }
            remainingVacations = totalVacations - usedVacations;
            
            LabelUsedDayCount.Text = usedVacations.ToString();
            LabelRemDayCount.Text = remainingVacations.ToString();
        }

        private void DataGridViewEmployeeVacations_SelectionChanged(object sender, EventArgs e)
        {            
            DateTimePickerVacationFrom.Value = DateTime.Now;
            DateTimePickerVacationTo.Value = DateTime.Now;
            if (DataGridViewEmployeeVacations.SelectedRows.Count == 0) return;
            if (_VacationDS == null) return;
            DataGridViewRow dgvr = DataGridViewEmployeeVacations.SelectedRows[0];
            DateTimePickerVacationFrom.Value = Convert.ToDateTime(dgvr.Cells[1].Value);
            DateTimePickerVacationTo.Value = Convert.ToDateTime(dgvr.Cells[2].Value);
        }

        private void ButtonDeleteVacation_Click(object sender, EventArgs e)
        {
            if (DataGridViewEmployee.SelectedRows.Count == 0)
            {
                MessageBox.Show("You must select an Employee", "Delete Vacation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (DataGridViewEmployeeVacations.SelectedRows.Count == 0)
            {
                MessageBox.Show("You must select a vacation", "Delete Vacation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            DialogResult result = MessageBox.Show("Are you sure that you want to delete the vacation?", "Delete Vacation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result != DialogResult.Yes) return;

            string vacationId = DataGridViewEmployeeVacations.SelectedRows[0].Cells[0].Value.ToString();
            string EmployeeId = DataGridViewEmployee.SelectedRows[0].Cells[0].Value.ToString();
            try
            {
                string sorgu = "DELETE FROM EmployeeVacations WHERE EmployeeVacationID = " + vacationId;
                if (DB.Uygula(sorgu, "ButtonDeleteVacation_Click"))
                {
                    MessageBox.Show("Vacation deleted.", "Delete Vacation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadVacations(EmployeeId);
                    ButtonNewVacation_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured while deleting vacation!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void CheckBoxPeriodFilter1_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBoxPeriodFilter1.Checked)
            {
                GroupBoxPeriod.Enabled = true;
                GroupBoxDate.Enabled = false;
                CheckBoxPeriodFilter2.Checked = false;
            }
            else if (!CheckBoxPeriodFilter1.Checked)
            {
                GroupBoxPeriod.Enabled = false;
                GroupBoxDate.Enabled = true;
                CheckBoxPeriodFilter2.Checked = true;
            }
        }

        private void CheckBoxPeriodFilter2_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBoxPeriodFilter2.Checked)
            {
                GroupBoxPeriod.Enabled = false;
                GroupBoxDate.Enabled = true;
                CheckBoxPeriodFilter1.Checked = false;
            }
            else if (!CheckBoxPeriodFilter2.Checked)
            {
                GroupBoxPeriod.Enabled = true;
                GroupBoxDate.Enabled = false;
                CheckBoxPeriodFilter1.Checked = true;
            }
        }
        private bool isPossibleActionsLoaded = false;
        private void DataGridViewActions_SelectionChanged(object sender, EventArgs e)
        {
            isPossibleActionsLoaded = false;
            LoadEmptyPossibleActions();
            if (DataGridViewActions.SelectedRows.Count > 0)
                LoadPossibleActions(DataGridViewActions.SelectedRows[0].Cells[0].Value.ToString());
            isPossibleActionsLoaded = true;
        }

        private void DataGridViewPossibleActions_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex == -1 || e.ColumnIndex == 0 || e.ColumnIndex == 2) return;

            //DataGridViewCheckBoxCell curCell = (DataGridViewCheckBoxCell)DataGridViewPossibleActions.Rows[e.RowIndex].Cells[e.ColumnIndex];
            //((DataGridViewCheckBoxCell)curCell.OwningRow.Cells[1]).Value = false;
        }

        private void DataGridViewPossibleActions_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex == -1 || isPossibleActionsLoaded == false) return;
            DataGridViewCheckBoxCell curCell = (DataGridViewCheckBoxCell)DataGridViewPossibleActions.Rows[e.RowIndex].Cells[e.ColumnIndex];
            String possibleActionId = curCell.OwningRow.Cells[0].Value.ToString();
            String actionId = DataGridViewActions.SelectedRows[0].Cells[0].Value.ToString();
            int val = Convert.ToInt32(curCell.Value);
            try
            {
                String sql = "";
                if (val == 0)
                {
                    sql = "Delete ActionFlow WHERE FirstActionID = " + actionId + " AND NextActionID = " + possibleActionId;
                }
                else if (val == 1)
                {
                    sql = "INSERT INTO ActionFlow(FirstActionID, NextActionID) VALUES(" + actionId + ", " + possibleActionId + ")";
                }
                if (!DB.Uygula(sql, ""))
                {
                    throw new Exception("DataGridViewPossibleActions_CellValueChanged");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured while updating the values!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void ButtonSubmit_Click(object sender, EventArgs e)
        {
            _EmployeeActionsDS = null;

            //creating a datatable for daily actions. It stores only selected day's actions.
            _EmployeeActionsDT = new DataTable("E");
            _EmployeeActionsDT.Columns.Add("ActionId");
            _EmployeeActionsDT.Columns.Add("Time");

            if(DataGridViewEmployeeList.SelectedRows.Count == 0)
            {
                MessageBox.Show("You must select an employee first!", "Employee Actions", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            String employeeId = DataGridViewEmployeeList.SelectedRows[0].Cells[0].Value.ToString();
            String sql = "SELECT * FROM EmployeeActions WHERE EmployeeID = " + employeeId;
            String sqlDatePart = "";
            if (CheckBoxPeriodFilter1.Checked)
            {
                if (RadioButtonAll.Checked)
                {
                }
                else if (RadioButtonYear.Checked)
                {
                    DateTime firstDayOfTheThisYear = Utility.GetFirstDayOfThisYear();
                    sqlDatePart = " AND ActionTime BETWEEN '" + String.Format(@"{0:yyyy-MM-dd HH:mm:ss}", firstDayOfTheThisYear) + "' ";
                    sqlDatePart += "AND '" + String.Format(@"{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now) + "' ";
                }
                else if (RadioButtonMonth.Checked)
                {
                    DateTime firstDayOfTheThisMonth = Utility.GetFirstDayOfThisMonth();
                    sqlDatePart = " AND ActionTime BETWEEN '" + String.Format(@"{0:yyyy-MM-dd HH:mm:ss}", firstDayOfTheThisMonth) + "' ";
                    sqlDatePart += "AND '" + String.Format(@"{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now) + "' ";
                }
                else if (RadioButtonWeek.Checked)
                {
                    DateTime firstDayOfTheThisWeek = Utility.GetFirstDayOfWeek(DateTime.Now);
                    sqlDatePart = " AND ActionTime BETWEEN '" + String.Format(@"{0:yyyy-MM-dd HH:mm:ss}", firstDayOfTheThisWeek) + "' ";
                    sqlDatePart += "AND '" + String.Format(@"{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now) + "' ";
                }
            }
            else //Custom period selected
            {
            }

            try
            {
                //the order by statement orders the action dates by descending but times in same day by descending
                sql = sql + sqlDatePart + " order by cast(floor(cast(ActionTime as float)) as datetime) desc, ActionTime asc";
                _EmployeeActionsDS = DB.Sorgula(sql, "ButtonSubmit_Click");
                if (_EmployeeActionsDS == null) throw new Exception("");
                DataGridViewEmployeeActions.Rows.Clear();

                String currentDate = "", previousDate = "";
                int actionColumnIndex = 3;
                int rowIndex = -1;
                foreach (DataRow dr in _EmployeeActionsDS.Tables[0].Rows)
                {
                    currentDate = Convert.ToDateTime(dr["ActionTime"]).ToShortDateString();
                    if (currentDate.Equals(previousDate))
                    {
                        DataGridViewEmployeeActions.Rows[rowIndex].Cells[actionColumnIndex].Value = String.Format("{0:HH:mm}", Convert.ToDateTime(dr["ActionTime"]));
                        //store EmployeeActionID for each action in the tag value of the cell to use in calculations
                        DataGridViewEmployeeActions.Rows[rowIndex].Cells[actionColumnIndex].Tag = dr["EmployeeActionID"].ToString();
                        actionColumnIndex++;
                    }
                    else
                    {
                        rowIndex++;
                        DataGridViewEmployeeActions.Rows.Add(currentDate, "", String.Format("{0:HH:mm}", Convert.ToDateTime(dr["ActionTime"])));
                        //store EmployeeActionID for each action in the tag value of the cell to use in calculations
                        DataGridViewEmployeeActions.Rows[rowIndex].Cells[2].Tag = dr["EmployeeActionID"].ToString();
                        actionColumnIndex = 3;
                        previousDate = currentDate;
                    }
                }

                CalculateTimeBlocks();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured while getting th in/out values!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
            }
            if (DataGridViewEmployeeActions.SelectedRows.Count > 0)
                DataGridViewEmployeeActions.SelectedRows[0].Selected = false;
        }

        private void CalculateTimeBlocks()
        {
            LoadActionFlows();
            foreach (DataGridViewRow dgvr in DataGridViewEmployeeActions.Rows)
            {
                int firstActionIndex = -1;
                int lastActionIndex = -1;
                for (int i = 2; i < 10; i++)
                {
                    if (i == 9)
                    {
                        lastActionIndex = 9;
                        break;
                    }
                    if (dgvr.Cells[i].Tag == null)
                    {
                        lastActionIndex = i - 1;
                        break;
                    }
                    if (firstActionIndex == -1)
                        firstActionIndex = i;
                }
                //there must be 4, 6 or 8 actions in a day. Otherwise there is an exception
                if ((lastActionIndex - firstActionIndex + 1) != 4 &&
                    (lastActionIndex - firstActionIndex + 1) != 6 &&
                    (lastActionIndex - firstActionIndex + 1) != 8)//illegal action count
                {
                    dgvr.Cells[0].Value = dgvr.Cells[0].Value.ToString() + "*";
                }
                else
                {
                    int firstEmployeeActionId = Convert.ToInt32(dgvr.Cells[firstActionIndex].Tag);
                    int lastEmployeeActionId = Convert.ToInt32(dgvr.Cells[lastActionIndex].Tag);

                    int firstActionId = Convert.ToInt32(_EmployeeActionsDS.Tables[0].Select("EmployeeActionID = " + firstEmployeeActionId)[0]["ActionID"].ToString());
                    int lastActionId = Convert.ToInt32(_EmployeeActionsDS.Tables[0].Select("EmployeeActionID = " + lastEmployeeActionId)[0]["ActionID"].ToString());

                    //first action must be in-morning and last action must be out-exit 
                    if (firstActionId != _InMorning && lastActionId != _OutExit)
                    {
                        dgvr.Cells[0].Value = dgvr.Cells[0].Value.ToString() + "*";
                        continue;
                    }

                    //check actionflows
                    for (int j = firstActionIndex; j <= lastActionIndex; j++)
                    {
                        int actionId = Convert.ToInt32(_EmployeeActionsDS.Tables[0].Select("EmployeeActionID = " + dgvr.Cells[j].Tag.ToString())[0]["ActionID"].ToString());
                        //if it is not the last action.
                        //because we check the last action on previous step
                        if (j != lastActionIndex)
                        {
                            int nextActionId = Convert.ToInt32(_EmployeeActionsDS.Tables[0].Select("EmployeeActionID = " + dgvr.Cells[j + 1].Tag.ToString())[0]["ActionID"].ToString());
                            int len = _ActionFlowDS.Tables[0].Select("FirstActionId = " + actionId + " AND NextActionId = " + nextActionId).Length;
                            if (len == 0)//it is not a legal action. because the action pair not defined.
                            {
                                dgvr.Cells[0].Value = dgvr.Cells[0].Value.ToString() + "*";
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void DataGridViewEmployeeList_SelectionChanged(object sender, EventArgs e)
        {
            DataGridViewEmployeeActions.Rows.Clear();
        }

        private void DataGridViewEmployeeActions_SelectionChanged(object sender, EventArgs e)
        {
            String labour = "";
            String extraLabour = "";
            String gDoctor = "";
            String sDoctor = "";
            String cusVend = "";
            String perIssues = "";

            DataGridViewDailyActions.Rows.Clear();
            DataGridViewTotalTime.Rows.Clear();
            _EmployeeActionsDT.Rows.Clear();
            if (DataGridViewEmployeeActions.SelectedRows.Count == 0)
                return;

            DataGridViewTotalTime.Rows.Add("Labour", "");
            DataGridViewTotalTime.Rows.Add("Extra Labour", "");
            DataGridViewTotalTime.Rows.Add("G.Doctor", "");
            DataGridViewTotalTime.Rows.Add("S.Doctor", "");
            DataGridViewTotalTime.Rows.Add("Customer/Vendor", "");
            DataGridViewTotalTime.Rows.Add("Personal Issues", "");

            for (int i = 0; i < 8; i++)
            {
                DataGridViewDailyActions.Rows.Add((i + 1) + ".Move", "", "");
            }

            for (int i = 2; i < 10; i++)
            {
                //no action
                if (DataGridViewEmployeeActions.SelectedRows[0].Cells[i].Tag == null)
                    break;
                DataGridViewDailyActions.Rows[i - 2].Cells[1].Value = DataGridViewEmployeeActions.SelectedRows[0].Cells[i].Value;
                int actionId = Convert.ToInt32(_EmployeeActionsDS.Tables[0].Select("EmployeeActionID = " + DataGridViewEmployeeActions.SelectedRows[0].Cells[i].Tag.ToString())[0]["ActionID"].ToString());
                
                String actionName = _ActionDS.Tables[0].Select("ActionId = " + actionId.ToString())[0]["ActionName"].ToString();
                DataGridViewDailyActions.Rows[i - 2].Cells[2].Value = actionName;
                _EmployeeActionsDT.Rows.Add(actionId, String.Format("{0:HH:mm}", DataGridViewEmployeeActions.SelectedRows[0].Cells[i].Value));
            }

            //If the selected date does not contain * character(if contains, it means that, in that day the actions are not valid)
            if (DataGridViewEmployeeActions.SelectedRows[0].Cells[0].Value.ToString().IndexOf('*') == -1)
            {
                //4 actions : In-morning, out lunch, in lunch, out-exit
                if (_EmployeeActionsDT.Rows.Count == 4 &&
                   _EmployeeActionsDT.Rows[0][0].ToString() == _InMorning.ToString() &&
                   _EmployeeActionsDT.Rows[1][0].ToString() == _OutLunch.ToString() &&
                   _EmployeeActionsDT.Rows[2][0].ToString() == _InLunch.ToString() &&
                   _EmployeeActionsDT.Rows[3][0].ToString() == _OutExit.ToString())
                {
                    TimeSpan beforeLunch = Convert.ToDateTime(_EmployeeActionsDT.Rows[1][1]).Subtract(Convert.ToDateTime(_EmployeeActionsDT.Rows[0][1]));
                    TimeSpan afterLunch = Convert.ToDateTime(_EmployeeActionsDT.Rows[3][1]).Subtract(Convert.ToDateTime(_EmployeeActionsDT.Rows[2][1]));
                    labour = beforeLunch.Add(afterLunch).Hours + ":" + beforeLunch.Add(afterLunch).Minutes;
                    DataGridViewTotalTime.Rows[0].Cells[1].Value = labour;
                }
                else
                {
                    DateTime inMorning = DateTime.MinValue, inPersonal = DateTime.MinValue, inGenDoc = DateTime.MinValue, inSpeDoc = DateTime.MinValue, inCusVen = DateTime.MinValue, inLunch = DateTime.MinValue;
                    DateTime outExit = DateTime.MinValue, outPersonal = DateTime.MinValue, outGenDoc = DateTime.MinValue, outSpeDoc = DateTime.MinValue, outCusVen = DateTime.MinValue, outLunch = DateTime.MinValue;


                    for (int i = 0; i < _EmployeeActionsDT.Rows.Count; i++)
                    {
                        if (_EmployeeActionsDT.Rows[i][0].ToString() == _InMorning.ToString())
                            inMorning = Convert.ToDateTime(_EmployeeActionsDT.Rows[i][1]);
                        else if (_EmployeeActionsDT.Rows[i][0].ToString() == _InSpecialPersonal.ToString())
                            inPersonal = Convert.ToDateTime(_EmployeeActionsDT.Rows[i][1]);
                        else if (_EmployeeActionsDT.Rows[i][0].ToString() == _InSpecialGenDocto.ToString())
                            inGenDoc = Convert.ToDateTime(_EmployeeActionsDT.Rows[i][1]);
                        else if (_EmployeeActionsDT.Rows[i][0].ToString() == _InSpecialSpeDoctor.ToString())
                            inSpeDoc = Convert.ToDateTime(_EmployeeActionsDT.Rows[i][1]);
                        else if (_EmployeeActionsDT.Rows[i][0].ToString() == _InSpecialCustomerVendor.ToString())
                            inCusVen = Convert.ToDateTime(_EmployeeActionsDT.Rows[i][1]);
                        else if (_EmployeeActionsDT.Rows[i][0].ToString() == _InLunch.ToString())
                            inLunch = Convert.ToDateTime(_EmployeeActionsDT.Rows[i][1]);
                        else if (_EmployeeActionsDT.Rows[i][0].ToString() == _OutExit.ToString())
                            outExit = Convert.ToDateTime(_EmployeeActionsDT.Rows[i][1]);
                        else if (_EmployeeActionsDT.Rows[i][0].ToString() == _OutSpecialPersonal.ToString())
                            outPersonal = Convert.ToDateTime(_EmployeeActionsDT.Rows[i][1]);
                        else if (_EmployeeActionsDT.Rows[i][0].ToString() == _OutSpecialGenDoctor.ToString())
                            outGenDoc = Convert.ToDateTime(_EmployeeActionsDT.Rows[i][1]);
                        else if (_EmployeeActionsDT.Rows[i][0].ToString() == _OutSpecialSpeDoctor.ToString())
                            outSpeDoc = Convert.ToDateTime(_EmployeeActionsDT.Rows[i][1]);
                        else if (_EmployeeActionsDT.Rows[i][0].ToString() == _OutSpecialCustomerVendor.ToString())
                            outCusVen = Convert.ToDateTime(_EmployeeActionsDT.Rows[i][1]);
                        else if (_EmployeeActionsDT.Rows[i][0].ToString() == _OutLunch.ToString())
                            outLunch = Convert.ToDateTime(_EmployeeActionsDT.Rows[i][1]);


                    }

                    if (inGenDoc.CompareTo(DateTime.MinValue) != 0 && outGenDoc.CompareTo(DateTime.MinValue) != 0)
                    {
                        DataGridViewTotalTime.Rows[2].Cells[1].Value = String.Format("{0:HH:mm}", new DateTime().Add(inGenDoc.Subtract(outGenDoc)));
                    }
                    if (inSpeDoc.CompareTo(DateTime.MinValue) != 0 && outSpeDoc.CompareTo(DateTime.MinValue) != 0)
                    {
                        DataGridViewTotalTime.Rows[3].Cells[1].Value = String.Format("{0:HH:mm}", new DateTime().Add(inSpeDoc.Subtract(outSpeDoc)));
                    }
                    if (inCusVen.CompareTo(DateTime.MinValue) != 0 && outCusVen.CompareTo(DateTime.MinValue) != 0)
                    {
                        DataGridViewTotalTime.Rows[4].Cells[1].Value = String.Format("{0:HH:mm}", new DateTime().Add(inCusVen.Subtract(outCusVen)));
                    }
                    if (inPersonal.CompareTo(DateTime.MinValue) != 0 && outPersonal.CompareTo(DateTime.MinValue) != 0)
                    {
                        DataGridViewTotalTime.Rows[5].Cells[1].Value = String.Format("{0:HH:mm}", new DateTime().Add(inPersonal.Subtract(outPersonal)));
                    }
                    if (inLunch.CompareTo(DateTime.MinValue) != 0 && outLunch.CompareTo(DateTime.MinValue) != 0)
                    {
                        TimeSpan lunch = inLunch.Subtract(outLunch);
                        TimeSpan inOut = outExit.Subtract(inMorning);

                        DataGridViewTotalTime.Rows[0].Cells[1].Value = String.Format("{0:HH:mm}", new DateTime().Add(inOut.Subtract(lunch)));
                    }
                }
            }
        }
    }
}
