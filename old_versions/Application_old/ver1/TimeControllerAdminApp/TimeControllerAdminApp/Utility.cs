﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace TimeControllerAdminApp
{
    class Utility
    {
        public static string InputFilter(string input)
        {
            while (input.Contains("'"))
                input = input.Replace("'", "");
            while (input.Contains("\""))
                input = input.Replace("\"", "");
            while (input.Contains("--"))
                input = input.Replace("--", "");
            while (input.Contains("#"))
                input = input.Replace("#", "");
            while (input.Contains("%"))
                input = input.Replace("%", "");

            if (input != null && input != string.Empty)
            {
                input = input.Trim();
            }
            return input;
        }

        public static DateTime GetFirstDayOfWeek(DateTime dayInWeek)
        {
            CultureInfo defaultCultureInfo = CultureInfo.CurrentCulture;
            DayOfWeek firstDay = defaultCultureInfo.DateTimeFormat.FirstDayOfWeek;
            return GetFirstDateOfWeek(dayInWeek, firstDay);
        }

        public static DateTime GetFirstDateOfWeek(DateTime dayInWeek, DayOfWeek firstDay)
        {
            DateTime firstDayInWeek = dayInWeek.Date;
            while (firstDayInWeek.DayOfWeek != firstDay)
                firstDayInWeek = firstDayInWeek.AddDays(-1);

            return firstDayInWeek;
        }

        public static DateTime GetFirstDayOfThisYear()
        {
            return new DateTime(DateTime.Now.Year, 1, 1);
        }

        public static DateTime GetFirstDayOfThisMonth()
        {
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }
    }
}
