﻿namespace TimeControllerApp
{
    partial class MaxForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing); 
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBoxInformation = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelTotalMinutesMustBeDone = new System.Windows.Forms.Label();
            this.labelTotalMinutesWorked = new System.Windows.Forms.Label();
            this.labelTotalMinutes = new System.Windows.Forms.Label();
            this.textBoxLastStatus = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxEntranceHour = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelShowTime = new System.Windows.Forms.Label();
            this.textBoxCodeNumber = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxNameSurname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxInOut = new System.Windows.Forms.GroupBox();
            this.buttonOutLunch = new System.Windows.Forms.Button();
            this.buttonIn = new System.Windows.Forms.Button();
            this.barcodebox = new System.Windows.Forms.TextBox();
            this.buttonOut = new System.Windows.Forms.Button();
            this.buttonApprove = new System.Windows.Forms.Button();
            this.buttonInMorning = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonInSpecial = new System.Windows.Forms.Button();
            this.buttonOutSpecial = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBoxInformation.SuspendLayout();
            this.groupBoxInOut.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupBoxInformation
            // 
            this.groupBoxInformation.Controls.Add(this.label6);
            this.groupBoxInformation.Controls.Add(this.label5);
            this.groupBoxInformation.Controls.Add(this.labelTotalMinutesMustBeDone);
            this.groupBoxInformation.Controls.Add(this.labelTotalMinutesWorked);
            this.groupBoxInformation.Controls.Add(this.labelTotalMinutes);
            this.groupBoxInformation.Controls.Add(this.textBoxLastStatus);
            this.groupBoxInformation.Controls.Add(this.label1);
            this.groupBoxInformation.Controls.Add(this.textBoxEntranceHour);
            this.groupBoxInformation.Controls.Add(this.label4);
            this.groupBoxInformation.Controls.Add(this.labelShowTime);
            this.groupBoxInformation.Controls.Add(this.textBoxCodeNumber);
            this.groupBoxInformation.Controls.Add(this.label3);
            this.groupBoxInformation.Controls.Add(this.textBoxNameSurname);
            this.groupBoxInformation.Controls.Add(this.label2);
            this.groupBoxInformation.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBoxInformation.Location = new System.Drawing.Point(960, 16);
            this.groupBoxInformation.Name = "groupBoxInformation";
            this.groupBoxInformation.Size = new System.Drawing.Size(301, 743);
            this.groupBoxInformation.TabIndex = 17;
            this.groupBoxInformation.TabStop = false;
            this.groupBoxInformation.Text = "Information";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(7, 560);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Minutos a Realizar : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(7, 530);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "Minutos Trabajados : ";
            // 
            // labelTotalMinutesMustBeDone
            // 
            this.labelTotalMinutesMustBeDone.AutoSize = true;
            this.labelTotalMinutesMustBeDone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelTotalMinutesMustBeDone.ForeColor = System.Drawing.Color.Red;
            this.labelTotalMinutesMustBeDone.Location = new System.Drawing.Point(164, 560);
            this.labelTotalMinutesMustBeDone.Name = "labelTotalMinutesMustBeDone";
            this.labelTotalMinutesMustBeDone.Size = new System.Drawing.Size(14, 20);
            this.labelTotalMinutesMustBeDone.TabIndex = 10;
            this.labelTotalMinutesMustBeDone.Text = ".";
            // 
            // labelTotalMinutesWorked
            // 
            this.labelTotalMinutesWorked.AutoSize = true;
            this.labelTotalMinutesWorked.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelTotalMinutesWorked.ForeColor = System.Drawing.Color.ForestGreen;
            this.labelTotalMinutesWorked.Location = new System.Drawing.Point(172, 530);
            this.labelTotalMinutesWorked.Name = "labelTotalMinutesWorked";
            this.labelTotalMinutesWorked.Size = new System.Drawing.Size(14, 20);
            this.labelTotalMinutesWorked.TabIndex = 9;
            this.labelTotalMinutesWorked.Text = ".";
            // 
            // labelTotalMinutes
            // 
            this.labelTotalMinutes.AutoSize = true;
            this.labelTotalMinutes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelTotalMinutes.Location = new System.Drawing.Point(7, 500);
            this.labelTotalMinutes.Name = "labelTotalMinutes";
            this.labelTotalMinutes.Size = new System.Drawing.Size(231, 20);
            this.labelTotalMinutes.TabIndex = 8;
            this.labelTotalMinutes.Text = "Mensual Report ( Excepto hoy )";
            // 
            // textBoxLastStatus
            // 
            this.textBoxLastStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxLastStatus.Location = new System.Drawing.Point(7, 290);
            this.textBoxLastStatus.Multiline = true;
            this.textBoxLastStatus.Name = "textBoxLastStatus";
            this.textBoxLastStatus.ReadOnly = true;
            this.textBoxLastStatus.Size = new System.Drawing.Size(285, 199);
            this.textBoxLastStatus.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(7, 266);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Último Movimiento:";
            // 
            // textBoxEntranceHour
            // 
            this.textBoxEntranceHour.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxEntranceHour.Location = new System.Drawing.Point(7, 233);
            this.textBoxEntranceHour.Name = "textBoxEntranceHour";
            this.textBoxEntranceHour.ReadOnly = true;
            this.textBoxEntranceHour.Size = new System.Drawing.Size(285, 24);
            this.textBoxEntranceHour.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(7, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Fecha de Entrada:";
            // 
            // labelShowTime
            // 
            this.labelShowTime.AutoSize = true;
            this.labelShowTime.Font = new System.Drawing.Font("MS Reference Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelShowTime.Location = new System.Drawing.Point(49, 40);
            this.labelShowTime.Name = "labelShowTime";
            this.labelShowTime.Size = new System.Drawing.Size(195, 35);
            this.labelShowTime.TabIndex = 0;
            this.labelShowTime.Text = "CurrentTime";
            // 
            // textBoxCodeNumber
            // 
            this.textBoxCodeNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxCodeNumber.Location = new System.Drawing.Point(7, 182);
            this.textBoxCodeNumber.Name = "textBoxCodeNumber";
            this.textBoxCodeNumber.ReadOnly = true;
            this.textBoxCodeNumber.Size = new System.Drawing.Size(285, 24);
            this.textBoxCodeNumber.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(7, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Codigo:";
            // 
            // textBoxNameSurname
            // 
            this.textBoxNameSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBoxNameSurname.Location = new System.Drawing.Point(7, 132);
            this.textBoxNameSurname.Name = "textBoxNameSurname";
            this.textBoxNameSurname.ReadOnly = true;
            this.textBoxNameSurname.Size = new System.Drawing.Size(285, 24);
            this.textBoxNameSurname.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(7, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nombre Apellido:";
            // 
            // groupBoxInOut
            // 
            this.groupBoxInOut.Controls.Add(this.buttonOutLunch);
            this.groupBoxInOut.Controls.Add(this.buttonIn);
            this.groupBoxInOut.Controls.Add(this.barcodebox);
            this.groupBoxInOut.Controls.Add(this.buttonOut);
            this.groupBoxInOut.Controls.Add(this.buttonApprove);
            this.groupBoxInOut.Controls.Add(this.buttonInMorning);
            this.groupBoxInOut.Controls.Add(this.buttonCancel);
            this.groupBoxInOut.Controls.Add(this.buttonInSpecial);
            this.groupBoxInOut.Controls.Add(this.buttonOutSpecial);
            this.groupBoxInOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxInOut.Location = new System.Drawing.Point(3, 16);
            this.groupBoxInOut.Name = "groupBoxInOut";
            this.groupBoxInOut.Size = new System.Drawing.Size(957, 743);
            this.groupBoxInOut.TabIndex = 18;
            this.groupBoxInOut.TabStop = false;
            // 
            // buttonOutLunch
            // 
            this.buttonOutLunch.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonOutLunch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOutLunch.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonOutLunch.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonOutLunch.Location = new System.Drawing.Point(574, 177);
            this.buttonOutLunch.Name = "buttonOutLunch";
            this.buttonOutLunch.Size = new System.Drawing.Size(163, 97);
            this.buttonOutLunch.TabIndex = 5;
            this.buttonOutLunch.Text = "Comida";
            this.buttonOutLunch.UseVisualStyleBackColor = false;
            this.buttonOutLunch.Click += new System.EventHandler(this.buttonOutLunch_Click);
            // 
            // buttonIn
            // 
            this.buttonIn.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonIn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonIn.Location = new System.Drawing.Point(54, 177);
            this.buttonIn.Name = "buttonIn";
            this.buttonIn.Size = new System.Drawing.Size(141, 206);
            this.buttonIn.TabIndex = 1;
            this.buttonIn.Text = "ENTRADA";
            this.buttonIn.UseVisualStyleBackColor = false;
            this.buttonIn.Click += new System.EventHandler(this.buttonIn_Click);
            // 
            // barcodebox
            // 
            this.barcodebox.Location = new System.Drawing.Point(370, 585);
            this.barcodebox.Name = "barcodebox";
            this.barcodebox.Size = new System.Drawing.Size(198, 20);
            this.barcodebox.TabIndex = 15;
            this.barcodebox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MaxForm_KeyPress);
            this.barcodebox.Leave += new System.EventHandler(this.barcodebox_Leave);
            // 
            // buttonOut
            // 
            this.buttonOut.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonOut.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonOut.Location = new System.Drawing.Point(743, 177);
            this.buttonOut.Name = "buttonOut";
            this.buttonOut.Size = new System.Drawing.Size(141, 206);
            this.buttonOut.TabIndex = 2;
            this.buttonOut.Text = "SALIDA";
            this.buttonOut.UseVisualStyleBackColor = false;
            this.buttonOut.Click += new System.EventHandler(this.buttonOut_Click);
            // 
            // buttonApprove
            // 
            this.buttonApprove.Location = new System.Drawing.Point(370, 411);
            this.buttonApprove.Name = "buttonApprove";
            this.buttonApprove.Size = new System.Drawing.Size(198, 78);
            this.buttonApprove.TabIndex = 12;
            this.buttonApprove.Text = "Aceptar";
            this.buttonApprove.UseVisualStyleBackColor = true;
            this.buttonApprove.Click += new System.EventHandler(this.buttonApprove_Click);
            // 
            // buttonInMorning
            // 
            this.buttonInMorning.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonInMorning.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInMorning.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonInMorning.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonInMorning.Location = new System.Drawing.Point(201, 177);
            this.buttonInMorning.Name = "buttonInMorning";
            this.buttonInMorning.Size = new System.Drawing.Size(163, 97);
            this.buttonInMorning.TabIndex = 3;
            this.buttonInMorning.Text = "Mañana";
            this.buttonInMorning.UseVisualStyleBackColor = false;
            this.buttonInMorning.Click += new System.EventHandler(this.buttonInMorning_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(370, 495);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(198, 78);
            this.buttonCancel.TabIndex = 11;
            this.buttonCancel.Text = "Cancelar";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonInSpecial
            // 
            this.buttonInSpecial.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonInSpecial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInSpecial.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonInSpecial.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonInSpecial.Location = new System.Drawing.Point(201, 280);
            this.buttonInSpecial.Name = "buttonInSpecial";
            this.buttonInSpecial.Size = new System.Drawing.Size(163, 103);
            this.buttonInSpecial.TabIndex = 4;
            this.buttonInSpecial.Text = "Especial";
            this.buttonInSpecial.UseVisualStyleBackColor = false;
            this.buttonInSpecial.Click += new System.EventHandler(this.buttonInSpecial_Click);
            // 
            // buttonOutSpecial
            // 
            this.buttonOutSpecial.BackColor = System.Drawing.Color.SteelBlue;
            this.buttonOutSpecial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOutSpecial.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonOutSpecial.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonOutSpecial.Location = new System.Drawing.Point(574, 280);
            this.buttonOutSpecial.Name = "buttonOutSpecial";
            this.buttonOutSpecial.Size = new System.Drawing.Size(163, 103);
            this.buttonOutSpecial.TabIndex = 6;
            this.buttonOutSpecial.Text = "Especial";
            this.buttonOutSpecial.UseVisualStyleBackColor = false;
            this.buttonOutSpecial.Click += new System.EventHandler(this.buttonOutSpecial_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBoxInOut);
            this.groupBox1.Controls.Add(this.groupBoxInformation);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1264, 762);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // MaxForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1264, 762);
            this.Controls.Add(this.groupBox1);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Name = "MaxForm";
            this.Text = "Time Controller";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.MaxForm_Shown);
            this.groupBoxInformation.ResumeLayout(false);
            this.groupBoxInformation.PerformLayout();
            this.groupBoxInOut.ResumeLayout(false);
            this.groupBoxInOut.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox groupBoxInformation;
        public System.Windows.Forms.GroupBox groupBoxInOut;
        private System.Windows.Forms.Button buttonOutLunch;
        private System.Windows.Forms.Label labelShowTime;
        private System.Windows.Forms.Button buttonIn;
        private System.Windows.Forms.TextBox barcodebox;
        private System.Windows.Forms.Button buttonOut;
        public System.Windows.Forms.Button buttonApprove;
        private System.Windows.Forms.Button buttonInMorning;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonInSpecial;
        private System.Windows.Forms.Button buttonOutSpecial;
        public System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxCodeNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxNameSurname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxEntranceHour;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxLastStatus;
        private System.Windows.Forms.Label labelTotalMinutes;
        private System.Windows.Forms.Label labelTotalMinutesWorked;
        private System.Windows.Forms.Label labelTotalMinutesMustBeDone;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    }
}

