﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace TimeControllerAdminApp
{
    class DB
    {

        private static SqlCommand cmd = new SqlCommand();
        private static SqlDataAdapter da = new SqlDataAdapter();
        public const string DATABASE = "ATI";
        public static string DBConnectionString = "Server=LENOVOE540\\SQLEXPRESS;Database=TimeControllerDB;User ID=sa;Password=486044;Trusted_Connection=False";
        private static string dbServer = "";
        public const string PASSWORD = "0okmju7ygvfr4eszaq1";
        public const string SERVER = "localhost";
        public const string USERNAME = "sa";

        // Methods

        public static DataSet Sorgula(string command, string sender)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(DBConnectionString))
                {
                    connection.Open();
                    if (connection.State == ConnectionState.Open)
                    {
                        using (SqlCommand command2 = new SqlCommand(command, connection))
                        {
                            using (SqlDataAdapter adapter = new SqlDataAdapter(command2))
                            {
                                using (DataSet set = new DataSet())
                                {
                                    adapter.Fill(set);
                                    return set;
                                }
                            }
                        }
                    }
                }
            }
            catch (SqlException exception)
            {
                if (exception.Number == 0x35)
                {
                    return null;
                }
                if (exception.Number == 0xe9)
                {
                    SqlConnection.ClearAllPools();
                    return null;
                }
            }
            catch (Exception exception2)
            {
                return null;
            }
            return null;
        }

        public static bool Uygula(string command, string sender)
        {
            bool flag;
            try
            {
                using (SqlConnection connection = new SqlConnection(DBConnectionString))
                {
                    connection.Open();
                    if (connection.State == ConnectionState.Open)
                    {
                        using (SqlCommand command2 = new SqlCommand(command, connection))
                        {
                            command2.CommandText = command;
                            command2.ExecuteNonQuery();
                            return true;
                        }
                    }
                    flag = false;
                }
            }
            catch (SqlException exception)
            {
                if (exception.Number == 0x35)
                {
                    return false;
                }
                if (exception.Number == 0xe9)
                {
                    SqlConnection.ClearAllPools();
                    return false;
                }
                flag = false;
            }
            catch (Exception exception2)
            {
                flag = false;
            }
            return flag;
        }

        public static bool UygulaCommand(SqlCommand command, string sender)
        {
            bool flag;
            try
            {
                using (SqlConnection connection = new SqlConnection(DBConnectionString))
                {
                    connection.Open();
                    if (connection.State == ConnectionState.Open)
                    {
                        command.Connection = connection;
                        command.ExecuteNonQuery();
                        return true;
                    }
                    flag = false;
                }
            }
            catch (SqlException exception)
            {
                if (exception.Number == 0x35)
                {
                    return false;
                }
                if (exception.Number == 0xe9)
                {
                    SqlConnection.ClearAllPools();
                    return false;
                }
                flag = false;
            }
            catch (Exception exception2)
            {
                flag = false;
            }
            return flag;
        }

        public static bool UygulaTrans(object[] transcommand, string sender)
        {
            SqlConnection connection = new SqlConnection(DBConnectionString);
            SqlTransaction transaction = null;
            try
            {
                connection.Open();
                if (connection.State == ConnectionState.Open)
                {
                    transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    for (int i = 0; i < transcommand.Length; i++)
                    {
                        using (SqlCommand command = new SqlCommand(transcommand[i].ToString(), connection, transaction))
                        {
                            command.ExecuteNonQuery();
                        }
                    }
                    transaction.Commit();
                    return true;
                }
                return false;
            }
            catch (SqlException exception)
            {
                if ((exception.Number != 0x35) && (exception.Number == 0xe9))
                {
                    SqlConnection.ClearAllPools();
                    return false;
                }
                return false;
            }
            catch (Exception exception2)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
            return false;
        }

        // Properties
        public static string DBServer
        {
            get
            {
                return dbServer;
            }
            set
            {
                dbServer = value;
            }
        }
    }
}
