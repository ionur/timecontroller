using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace TimeControllerAdminApp
{
    public partial class FormReport : Form
    {
        public FormReport()
        {
            InitializeComponent();
        }

        System.Threading.Thread th = null;

        private void Form1_Load(object sender, EventArgs e)
        {
            th = new System.Threading.Thread(new System.Threading.ThreadStart(RaporHazirla));
            th.Start();
        }

        #region Aray�z
        private void butYazdir_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.PrintReport();
        }

        private void cmbView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbView.Text != "")
            {
                int zoom = 100;
                try
                {
                    zoom = Convert.ToInt32(cmbView.Text.Replace("%", ""));
                }
                catch
                {
                    zoom = 100;
                }
                crystalReportViewer1.Zoom(zoom);
            }
        }

        private void butKaydet_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.ExportReport();
        }

        private void butGeri_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.ShowNthPage(crystalReportViewer1.GetCurrentPageNumber() - 1);
            labelSayfa.Text = crystalReportViewer1.GetCurrentPageNumber() + "/n";
        }

        private void butIleri_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.ShowNthPage(crystalReportViewer1.GetCurrentPageNumber() + 1);
            labelSayfa.Text = crystalReportViewer1.GetCurrentPageNumber() + "/n";
        }

        private void butBas_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.ShowFirstPage();
        }

        private void butSon_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.ShowLastPage();
        }

        private void butAra_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.SearchForText(textAra.Text);
        }
        private void butDokuman_Click(object sender, EventArgs e)
        {
           // reportDocument1.SaveAs(".\\temp\\temp.rpt", true);
        }

        private void textAra_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                butAra_Click(null, null);
        }

        private void butZoomIn_Click(object sender, EventArgs e)
        {
            if (cmbView.SelectedIndex + 1 > cmbView.Items.Count - 1) return;

            cmbView.SelectedIndex = cmbView.SelectedIndex + 1;
        }

        private void ButZoomOut_Click(object sender, EventArgs e)
        {
            if (cmbView.SelectedIndex - 1 < 0) return;

            cmbView.SelectedIndex = cmbView.SelectedIndex - 1;
        }

        private void viewDoldur()
        {
            for (int i = 25; i < 200; i += 25)
                cmbView.Items.Add("%" + i.ToString());
            for (int i = 200; i < 400; i += 100)
                cmbView.Items.Add("%" + i.ToString());

            cmbView.ComboBox.Text = "%100";
        }
        #endregion Aray�z

        private void FormArayuz_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (th != null) th.Abort();
            reportDocument1.Dispose();
        }

        public void setReport(CrystalDecisions.CrystalReports.Engine.ReportDocument rpt)
        {
            reportDocument1 = rpt;
        }

        private void RaporHazirla()
        {
            if (!InvokeRequired)
            {
                viewDoldur();
                crystalReportViewer1.ReportSource = reportDocument1;
                crystalReportViewer1.Zoom(2);
                ViewerTabSakla();
            }
            else
            {
                this.Invoke(new MethodInvoker(RaporHazirla));
            }
        }

        private void ViewerTabSakla()
        {
            try
            {
                foreach (Control control in crystalReportViewer1.Controls)
                {
                    if (string.Compare(control.GetType().Name, "PageView", true, System.Globalization.CultureInfo.InvariantCulture) == 0)
                    {
                        TabControl tab = (TabControl)((CrystalDecisions.Windows.Forms.PageView)control).Controls[0];

                        tab.ItemSize = new Size(0, 1);
                        tab.SizeMode = TabSizeMode.Fixed;
                        tab.Appearance = TabAppearance.Buttons;
                        break;
                    }
                }
            }
            catch { return; }
        }
    }
}
