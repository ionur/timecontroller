namespace TimeControllerAdminApp
{
    partial class FormReport
    {
        private System.ComponentModel.IContainer components = null;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormReport));
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.reportDocument1 = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.butKaydet = new System.Windows.Forms.ToolStripButton();
            this.butYazdir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.cmbView = new System.Windows.Forms.ToolStripComboBox();
            this.butBas = new System.Windows.Forms.ToolStripButton();
            this.butGeri = new System.Windows.Forms.ToolStripButton();
            this.labelSayfa = new System.Windows.Forms.ToolStripLabel();
            this.butIleri = new System.Windows.Forms.ToolStripButton();
            this.butSon = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.textAra = new System.Windows.Forms.ToolStripTextBox();
            this.butAra = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.butZoomIn = new System.Windows.Forms.ToolStripButton();
            this.ButZoomOut = new System.Windows.Forms.ToolStripButton();
            this.butDokuman = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            //this.crystalReportViewer1.DisplayStatusBar = false;
            this.crystalReportViewer1.DisplayToolbar = false;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 35);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.SelectionFormula = "";
            this.crystalReportViewer1.ShowGroupTreeButton = false;
            this.crystalReportViewer1.Size = new System.Drawing.Size(742, 531);
            this.crystalReportViewer1.TabIndex = 0;
            //this.crystalReportViewer1.ViewTimeSelectionFormula = "";
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.butKaydet,
            this.butYazdir,
            this.toolStripSeparator2,
            this.cmbView,
            this.butBas,
            this.butGeri,
            this.labelSayfa,
            this.butIleri,
            this.butSon,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.textAra,
            this.butAra,
            this.toolStripSeparator3,
            this.butZoomIn,
            this.ButZoomOut,
            this.butDokuman});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(742, 35);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // butKaydet
            // 
            this.butKaydet.AutoSize = false;
            this.butKaydet.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butKaydet.Image = ((System.Drawing.Image)(resources.GetObject("butKaydet.Image")));
            this.butKaydet.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.butKaydet.Margin = new System.Windows.Forms.Padding(2, 1, 0, 2);
            this.butKaydet.Name = "butKaydet";
            this.butKaydet.Size = new System.Drawing.Size(40, 30);
            this.butKaydet.Text = "Save";
            this.butKaydet.Click += new System.EventHandler(this.butKaydet_Click);
            // 
            // butYazdir
            // 
            this.butYazdir.AutoSize = false;
            this.butYazdir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butYazdir.Image = ((System.Drawing.Image)(resources.GetObject("butYazdir.Image")));
            this.butYazdir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.butYazdir.Name = "butYazdir";
            this.butYazdir.Size = new System.Drawing.Size(40, 30);
            this.butYazdir.Text = "Print";
            this.butYazdir.Click += new System.EventHandler(this.butYazdir_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 35);
            // 
            // cmbView
            // 
            this.cmbView.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbView.Name = "cmbView";
            this.cmbView.Size = new System.Drawing.Size(75, 35);
            this.cmbView.ToolTipText = "View";
            this.cmbView.SelectedIndexChanged += new System.EventHandler(this.cmbView_SelectedIndexChanged);
            // 
            // butBas
            // 
            this.butBas.AutoSize = false;
            this.butBas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butBas.Image = ((System.Drawing.Image)(resources.GetObject("butBas.Image")));
            this.butBas.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.butBas.Name = "butBas";
            this.butBas.Size = new System.Drawing.Size(35, 30);
            this.butBas.Text = "First Page";
            this.butBas.Click += new System.EventHandler(this.butBas_Click);
            // 
            // butGeri
            // 
            this.butGeri.AutoSize = false;
            this.butGeri.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butGeri.Image = ((System.Drawing.Image)(resources.GetObject("butGeri.Image")));
            this.butGeri.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.butGeri.Name = "butGeri";
            this.butGeri.Size = new System.Drawing.Size(35, 30);
            this.butGeri.Text = "Previous";
            this.butGeri.Click += new System.EventHandler(this.butGeri_Click);
            // 
            // labelSayfa
            // 
            this.labelSayfa.Name = "labelSayfa";
            this.labelSayfa.Size = new System.Drawing.Size(24, 32);
            this.labelSayfa.Text = "z/n";
            // 
            // butIleri
            // 
            this.butIleri.AutoSize = false;
            this.butIleri.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butIleri.Image = ((System.Drawing.Image)(resources.GetObject("butIleri.Image")));
            this.butIleri.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.butIleri.Name = "butIleri";
            this.butIleri.Size = new System.Drawing.Size(35, 30);
            this.butIleri.Text = "Next";
            this.butIleri.Click += new System.EventHandler(this.butIleri_Click);
            // 
            // butSon
            // 
            this.butSon.AutoSize = false;
            this.butSon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butSon.Image = ((System.Drawing.Image)(resources.GetObject("butSon.Image")));
            this.butSon.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.butSon.Name = "butSon";
            this.butSon.Size = new System.Drawing.Size(35, 30);
            this.butSon.Text = "Last Page";
            this.butSon.Click += new System.EventHandler(this.butSon_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 35);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(42, 32);
            this.toolStripLabel1.Text = "Search";
            // 
            // textAra
            // 
            this.textAra.Name = "textAra";
            this.textAra.Size = new System.Drawing.Size(150, 35);
            this.textAra.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textAra_KeyDown);
            // 
            // butAra
            // 
            this.butAra.AutoSize = false;
            this.butAra.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butAra.Image = ((System.Drawing.Image)(resources.GetObject("butAra.Image")));
            this.butAra.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.butAra.ImageTransparentColor = System.Drawing.Color.White;
            this.butAra.Name = "butAra";
            this.butAra.Size = new System.Drawing.Size(40, 30);
            this.butAra.Text = "Search";
            this.butAra.ToolTipText = "Search";
            this.butAra.Click += new System.EventHandler(this.butAra_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 35);
            // 
            // butZoomIn
            // 
            this.butZoomIn.AutoSize = false;
            this.butZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("butZoomIn.Image")));
            this.butZoomIn.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.butZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butZoomIn.Name = "butZoomIn";
            this.butZoomIn.Size = new System.Drawing.Size(40, 30);
            this.butZoomIn.Text = "Zoom Out";
            this.butZoomIn.Click += new System.EventHandler(this.butZoomIn_Click);
            // 
            // ButZoomOut
            // 
            this.ButZoomOut.AutoSize = false;
            this.ButZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("ButZoomOut.Image")));
            this.ButZoomOut.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButZoomOut.Name = "ButZoomOut";
            this.ButZoomOut.Size = new System.Drawing.Size(40, 30);
            this.ButZoomOut.Text = "Zoom In";
            this.ButZoomOut.Click += new System.EventHandler(this.ButZoomOut_Click);
            // 
            // butDokuman
            // 
            this.butDokuman.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.butDokuman.Image = ((System.Drawing.Image)(resources.GetObject("butDokuman.Image")));
            this.butDokuman.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butDokuman.Name = "butDokuman";
            this.butDokuman.Size = new System.Drawing.Size(94, 19);
            this.butDokuman.Text = "Save Document";
            this.butDokuman.Visible = false;
            this.butDokuman.Click += new System.EventHandler(this.butDokuman_Click);
            // 
            // FormReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 566);
            this.Controls.Add(this.crystalReportViewer1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "FormReport";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Print";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormArayuz_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private CrystalDecisions.CrystalReports.Engine.ReportDocument reportDocument1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton butKaydet;
        private System.Windows.Forms.ToolStripButton butYazdir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripComboBox cmbView;
        private System.Windows.Forms.ToolStripButton butBas;
        private System.Windows.Forms.ToolStripButton butGeri;
        private System.Windows.Forms.ToolStripLabel labelSayfa;
        private System.Windows.Forms.ToolStripButton butIleri;
        private System.Windows.Forms.ToolStripButton butSon;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox textAra;
        private System.Windows.Forms.ToolStripButton butAra;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton butDokuman;
        private System.Windows.Forms.ToolStripButton ButZoomOut;
        private System.Windows.Forms.ToolStripButton butZoomIn;
    }
}

